﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BarManager;

public class Program : MonoBehaviour {

	// Use this for initialization
	void Start () {
		/// Create LedBarManager object.
		/// Port name is set based on my Mac, probabily you should change it with COM6.
		LedBarManager lb = new LedBarManager("/dev/cu.usbmodem1411");

		/// Set the color for the teams
		lb.SetTeamColor(1, "FF00FF");
		lb.SetTeamColor(2, "453400");
		/// Ready, set, go. 
		/// 12 is the total number of points to win the game.
		lb.StartProgressBar(12);


		/// Add some points here and there.
		/// Not possible to overflow: if 10 is the maximum number of points
		/// each team can go up to 10.
		/// lb.AddPoint(teamNumber) will add 1 point by default.
		/// ATTENTION: points is different from the led number which is 
		/// determined according to the percentage...
		lb.AddPoint(1, 4);
		lb.AddPoint(2, 3);

		/// two effects at a time?
		/// not a problem bro!
		System.Threading.Thread.Sleep(1000);
		lb.ShowEffect(1, 2, 10000);
		System.Threading.Thread.Sleep(1000);
		lb.ShowEffect(0, 0, 5000);


		System.Threading.Thread.Sleep(10000);


		/// This work as a reset.
		lb.StartProgressBar(10);

		System.Threading.Thread.Sleep(1000);
		lb.AddPoint(2, 3);
		lb.AddPoint(1, 1);


		/// Instantly switches off everything.
		System.Threading.Thread.Sleep(2000);
		lb.SwitchOff();


		/// Alla fine di tutto tutto tutto dovresti fare così
		/// ma puoi anche non farlo...
		lb.Destroy ();




	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
