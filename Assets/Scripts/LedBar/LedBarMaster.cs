﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BarManager;

public class LedBarMaster : MonoBehaviour {

	private static LedBarManager lb = null;

	public static void Initialize (int pointsToWin)
	{
		/// Create LedBarManager object.
		/// Port name is set based on my Mac, probabily you should change it with COM6.
		lb = new LedBarManager("COM3");

		/// Set the color for the teams
		if (lb != null) {
			lb.SetTeamColor (1, "FF00FF");
			lb.SetTeamColor (2, "453400");
		}

		if (lb != null) {
			lb.StartProgressBar (pointsToWin);
		}
	}

	public static void AddPoints(int team, int points)
	{
		if (lb != null) {
			lb.AddPoint (team, points);
		}
	}

	public static void ShowEffect(int team, int effect, int time)
	{
		if (lb != null) {
			System.Threading.Thread.Sleep (1000);
			lb.ShowEffect (team - 1, effect, time);
		}
	}

	public static void SwitchOff()
	{
		if (lb != null) {
			lb.SwitchOff ();
		}
	}

	void OnDestroy ()
	{
		if (lb != null) {
			lb.SwitchOff ();
			lb.Destroy ();
		}
	}

	void OnApplicationQuit ()
	{
		if (lb != null) {
			lb.SwitchOff ();
			lb.Destroy ();
		}
	}

}
