﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour {

	GameObject[] pauseObjects;

	// Use this for initialization
	void Awake () {
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag ("ShowOnPause");
		hidePaused ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.B) && Time.timeScale == 1) {
			pauseControl ();
		}
	}

	public void pauseControl()
	{
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
			showPaused ();
		} else if (Time.timeScale == 0) {
			Time.timeScale = 1;
			hidePaused ();
		}
	}

	private void showPaused()
	{
		foreach (GameObject g in pauseObjects) {
			g.SetActive (true);
		}
	}

	private void hidePaused()
	{
		foreach (GameObject g in pauseObjects) {
			g.SetActive (false);
		}
	}

	public void LoadLevel(string level)
	{
		ChoiceStatus.setProgressionStory (0);
		SceneManager.LoadScene (level);
	}
}
