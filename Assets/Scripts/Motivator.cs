﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motivator : MonoBehaviour {

	public int interval;
	public AudioSource source;
	public AudioClip[] clips;

	// Use this for initialization
	void Start () {
		StartCoroutine (Motivate (interval));
	}
	
	private IEnumerator Motivate(int seconds)
	{
		yield return new WaitForSeconds (seconds);
		if (!source.isPlaying) {
			source.clip = PickAClip ();
			source.Play ();
		}
		StartCoroutine (Motivate (seconds));
	}

	private AudioClip PickAClip()
	{
		return clips[Random.Range(0, clips.Length)];
	}
}
