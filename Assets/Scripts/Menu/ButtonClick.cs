﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClick : MonoBehaviour {

	public AudioSource audSource;
	public AudioClip soundClick;
	public AudioClip soundSelect;
	public AudioClip soundQuit;


	/// <summary>
	/// Plaies the click of the button, when you press it.
	/// </summary>
	public void playClick(){
		audSource.PlayOneShot (soundClick);
	}


	/// <summary>
	/// Plaies the selection when you navigate in the menu.
	/// </summary>
	public void playSelection(){
		audSource.PlayOneShot (soundSelect);
	}


	/// <summary>
	/// Plaies the sound of the quit button.
	/// </summary>
	public void playQuit(){
		audSource.PlayOneShot (soundQuit);
	}


}
