﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSupport : MonoBehaviour {


	/// <summary>
	/// Loads the scene whose name is passed as a parameter
	/// </summary>
	/// <param name="sceneName">Name of the next scene to load</param> 
	public void LoadScene(string sceneName){
		StartCoroutine (LoadingScene (sceneName));
	}


	/// <summary>
	/// Wait some time before load the scene to let visible the animation "press the button" 
	/// </summary>
	IEnumerator LoadingScene (string sceneName){
		yield return new WaitForSeconds (0.3f);
		SceneManager.LoadScene (sceneName);
	}


	/// <summary>
	/// Loads the right scene when you are in the level screen and you want to come back in the navigation of the menu
	/// </summary>
	public void GoBackFromLevelScene(){
		if (ChoiceStatus.story == "Wood") {
			LoadScene("MenuGameStory");
		}
		if (ChoiceStatus.story == "Lights"){
			LoadScene("MenuIniziale");
		}
		if (ChoiceStatus.story == "Memo") {
			LoadScene("MenuIniziale");
		}
	}


	/// <summary>
	/// Save the name of the game choosen by the user in ChoiceStatus class.
	/// </summary>
	public void setStory(string s){
		ChoiceStatus.setStory (s);
	}


	/// <summary>
	/// Save the level of the game choosen by the user in ChoiceStatus class.
	/// </summary>
	/// <param name="l">L.</param>
	public void setLevel(int l){
		ChoiceStatus.setLevel(l);
	}

	/// <summary>
	/// Save the scene choosen in the scene screen by the user in the case of the game story
	/// </summary>
	/// <param name="p">Number of the scene selected, 1 for scene 1 etc.</param> 
	public void setProgressionStory(int p){
		ChoiceStatus.setProgressionStory (p);
	}


	/// <summary>
	/// Load the right game based on the choice made by user in the menu. Used when you are in the level screen.
	/// </summary>
	public void loadRightScene(){
		string sceneName;
		if (ChoiceStatus.story == "Wood") {
			sceneName = "WoodStory1";
			setProgressionStory (0);
			LedBarMaster.Initialize (8);
			ChoiceStatus.numberOfPlayers = 3;
			LoadScene(sceneName);
		}
		if (ChoiceStatus.story == "Lights") {
			sceneName = "CollaborativoLuci_1";
			LedBarMaster.Initialize (4);
			LoadScene (sceneName);
		}
		if (ChoiceStatus.story == "Memo") {
			sceneName = "MemoPositioning"; 
			setProgressionStory (0);
			LedBarMaster.Initialize (4);
			ChoiceStatus.numberOfPlayers = 2;
			ChoiceStatus.mode = "Collaborative";
			LoadScene (sceneName);

		}
	}


	/// <summary>
	/// Stop the Menu song. Used when you load the game
	/// </summary>
	public void stopAudioMenu () {
		AudioManagerMenu.getInstance ().GetComponent<AudioSource> ().Stop ();
		Destroy (AudioManagerMenu.getInstance ().gameObject);
	}


}





