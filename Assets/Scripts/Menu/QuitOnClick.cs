﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitOnClick : MonoBehaviour {

	public void Quit(){
		#if UNITY_EDITOR //se siamo in unity editor faccio una cosa, altrimento un'altra
			StartCoroutine (QuitOnUnity ());
			
		#else
			StartCoroutine (QuitOutOfUnity ());
			
		#endif
	}

	IEnumerator QuitOnUnity (){
		yield return new WaitForSeconds (0.3f);
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false; //exit play mode
		#endif
	}

	IEnumerator QuitOutOfUnity (){
		yield return new WaitForSeconds (0.3f);
		Application.Quit(); //if grey we are running in the editor
	}
}
