﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {

	public EventSystem eventSystem;
	public GameObject selectedObject;

	private bool buttonSelected;
	
	// Update is called once per frame
	void Update () 
	{
		//see if player give some input from the keyboard or controller
		if(Input.GetAxisRaw("Horizontal") != 0 && buttonSelected == false) {//some movement detected on the horiz axes even from game pad or keyboard
			eventSystem.SetSelectedGameObject(selectedObject);
			buttonSelected = true;
		}	
	}

	private void OnDisable() //when the game object is not selected
	{
		//we deactivated the object
		buttonSelected = false;
	}
}
