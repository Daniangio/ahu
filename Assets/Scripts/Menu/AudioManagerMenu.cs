﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerMenu : MonoBehaviour {

	private static AudioManagerMenu instance = null;


	public static AudioManagerMenu getInstance(){
		return instance;
	}


	/// <summary>
	/// Allow only one instance of the AudioManager, that remains between scenes.
	/// </summary>
	void Awake (){
		if ((instance != null) && (instance != this)) {
			Destroy (this.gameObject);
		} else {
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}


} 

