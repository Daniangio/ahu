﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BiggerButtons : MonoBehaviour {

	private float scaleAmount = 1.2f;
	public GameObject selected;
	private Vector3 bigger;
	private Vector3 normal;

	void Start() {
		bigger = new Vector3 (scaleAmount, scaleAmount, 1f);
		normal =  new Vector3 (1f, 1f, 1f);
		if (selected == null) {
			return;
		}
	}
	

	public void setBiggerScale(){
		selected.transform.localScale = bigger;
	}

	public void setNormalScale(){
		selected.transform.localScale = normal;
	}
}
