﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FrameScrMemo : MonoBehaviour {

	private SceneMasterScrMemo sceneMaster;
	private FramesOrganizerCone framesOrganizer;

	private bool isSelected = false;
	public int fieldNumber = 0;
	public bool isRounded; // inutile ma mettere intero con numero per associazioni con altre carte->id
	public bool isTurning = false;
	public bool isCovering = false;
	public int id; //identificativo carta 
	public SpriteRenderer element; //disegno in mezzo ad una carta
	public SpriteRenderer context;
	public SpriteRenderer card;

	public int speed = 1;
	private int count; //posizione nel vettore frameDisposed
	public string sector;

	private float maxDistance_x;
	private float maxDistance_y;

	private Color colorMax;
	private Color colorMin;
	void Start() {
		colorMin = new Color (1, 1, 1, 0);
		colorMax = new Color (1, 1, 1, 1);
	}



	/// <summary>
	/// Initialize the specified framesOrganizerName, spriteName, isCorrect and count in case of Memo.
	/// </summary>
	/// <param name="framesOrganizerName">Frames organizer name.</param>
	/// <param name="spriteName">Sprite name.</param>
	/// <param name="isCorrect">If set to <c>true</c> is correct.</param>
	/// <param name="count">Count.</param>
	public void Initialize(string framesOrganizerName, string spriteName, int id, int count, string sector, bool isRounded) //isRounded forse non serve perchè già in prefab
	{
		sceneMaster = GameObject.Find ("SceneMaster").GetComponent<SceneMasterScrMemo> (); //vedi se serve realmente
		framesOrganizer = GameObject.Find (framesOrganizerName).GetComponent<FramesOrganizerCone> ();
	//Load element sprite
		int index = (int)Char.GetNumericValue(spriteName[spriteName.Length-1]) + 1;
		spriteName = spriteName.Substring(0,spriteName.Length - 2);
		string filePath = "Sprites/" + spriteName;
		var sprites  = Resources.LoadAll(filePath);
		element.sprite = (Sprite)sprites[index];
		if (element.sprite == null)
			Debug.Log ("SPRITE NON CARICATA");

		//Move the element in the center of the context
		element.transform.position = new Vector3 (transform.position.x, transform.position.y);

		//Load and adapt to screen the context sprite
		filePath = "Sprites/" + "rect";
		var spr = Resources.LoadAll(filePath);
		Sprite contextSpr = (Sprite)spr[1];
		context.sprite = contextSpr;
		context.color = new Color (0, 0, 0, 0.2f);

		filePath = "Sprites/" + "redCard";
		spr = Resources.LoadAll(filePath);
		Sprite cardSpr = (Sprite)spr[1];
		card.sprite = cardSpr;

		card.transform.position = new Vector3 (transform.position.x, transform.position.y); 

		this.isRounded = isRounded; //potrebbe non servire perchè in prefab è già segnato false per tutti, quindi inutile passarlo a meno che altro gioco si inizia con qualche carta scoperta, tipo scopa 

		this.isSelected = false;
		this.sector = sector;
		this.count = count;
		this.id = id;
		this.isRounded = isRounded;// due volte?

		ResizeSpriteToScreen (context, 0.95f, false);
		ResizeSpriteToScreen (card, 0.85f, false);
		ResizeSpriteToScreen (element, 0.85f, true);
	}


	/// <summary>
	/// Every time it finds the nearest player, select and deselect the frame. (ogni framescr lo fa, secondo me troppa computazione)
	/// </summary>
	void Update()
	{
		if (fieldNumber > 0) {
			GameObject nearestPlayer = FindClosest (transform.position, "Player");
			if (IsOverMe (nearestPlayer.transform.position) && !isSelected) {
				Select (); 
				// passa questo frame al player! MODIFiCA 23/02
				//PlayerScr player = nearestPlayer.GetComponent<PlayerScr> ();
				nearestPlayer.GetComponent<PlayerScr> ().setFrame (this);
			}
		
			if (!IsOverMe (nearestPlayer.transform.position) && isSelected) {
				Select ();
			}
		}
		if (isTurning == true) {
			transform.GetChild(2).GetComponent<SpriteRenderer> ().color = Color.Lerp (transform.GetChild(2).GetComponent<SpriteRenderer> ().color, colorMin, Time.deltaTime * speed);
			if (transform.GetChild(2).GetComponent<SpriteRenderer> ().color.a < 0.1f){
				transform.GetChild(2).GetComponent<SpriteRenderer> ().color = colorMin;
				isTurning = false;
			} 
		}
		if (isCovering == true) {
			transform.GetChild(2).GetComponent<SpriteRenderer> ().color = Color.Lerp (transform.GetChild(2).GetComponent<SpriteRenderer> ().color, colorMax, Time.deltaTime * speed);
			if (transform.GetChild(2).GetComponent<SpriteRenderer> ().color.a > 0.95f){
				transform.GetChild(2).GetComponent<SpriteRenderer> ().color = colorMax;
				isCovering = false;
			} 
		}
	}

	private void ResizeSpriteToScreen(SpriteRenderer sr, float size, bool keepRatio) {
		if (sr == null)
			return;

		var worldScreenHeight = Camera.allCameras[fieldNumber - 1].orthographicSize * 2;
		var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		Vector2 local_sprite_size = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

		float frame_width = (worldScreenWidth - framesOrganizer.border_left - framesOrganizer.border_right - framesOrganizer.center) / framesOrganizer.horizontalFrames;
		float frame_height = (worldScreenHeight - framesOrganizer.border_top) / framesOrganizer.verticalFrames;
		float myScale_x;
		float myScale_y;
		if (keepRatio) {
			if (local_sprite_size.x < local_sprite_size.y) {
				myScale_y = (1 / local_sprite_size.y) * frame_height * size;
				myScale_x = myScale_y;
				if (local_sprite_size.x * myScale_x > frame_width) {
					myScale_x = (1 / local_sprite_size.x) * frame_width * size;
					myScale_y = myScale_x;
					if (local_sprite_size.y * myScale_y > frame_height) {
						myScale_x = (1 / local_sprite_size.x) * frame_width * size;
						myScale_y = (1 / local_sprite_size.y) * frame_height * size;
					}
				}
			} else {
				myScale_x = (1 / local_sprite_size.x) * frame_width * size;
				myScale_y = myScale_x;
			}
		}
		else {
			myScale_x = (1 / local_sprite_size.x) * frame_width * size;
			myScale_y = (1 / local_sprite_size.y) * frame_height * size;
		}
		sr.transform.localScale = new Vector3 (myScale_x, myScale_y, 1);

			//differenzazioni livelli, ora faccio solo caso semplice

			float camerax = Camera.allCameras [fieldNumber - 1].transform.position.x;
			float cameray = Camera.allCameras [fieldNumber - 1].transform.position.y;
			var yFirstCard = cameray + worldScreenHeight / 2 - framesOrganizer.border_top - frame_height/2; 
		//left sector
		if (sector == "left") {
				switch (count) {
				case 0:
					transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2), yFirstCard, 0);
					break;

				case 1: 
					transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2) , yFirstCard - frame_height, 0);
					break;
				case 2:
				transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2) - frame_width, yFirstCard - frame_height, 0);
					break;
				case 3:
					transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2), yFirstCard - frame_height - frame_height, 0);
					break;
				case 4:
					transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2) - frame_width, yFirstCard - frame_height - frame_height, 0);
					break;
				case 5:
					transform.position = new Vector3 (camerax - framesOrganizer.center / 2 - (frame_width / 2) - frame_width - frame_width, yFirstCard - frame_height - frame_height, 0);
					break;
				}

			} 
			//right sector
		if (sector == "right") {
				switch (count) {
				case 0:
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2), yFirstCard, 0);
					break;
				case 1: 
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2) , yFirstCard - frame_height, 0);
					break;
				case 2:
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2) + frame_width, yFirstCard - frame_height, 0);
					break;
				case 3:
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2), yFirstCard - frame_height - frame_height, 0);
					break;
				case 4:
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2) + frame_width, yFirstCard - frame_height - frame_height, 0);
					break;
				case 5:
					transform.position = new Vector3 (camerax + framesOrganizer.center / 2 + (frame_width / 2) + frame_width + frame_width, yFirstCard - frame_height - frame_height, 0);
					break;
				}

			}

		maxDistance_x = local_sprite_size.x * myScale_x / 2;
		maxDistance_y = local_sprite_size.y * myScale_y / 2;


	}


	/// <summary>
	/// Determines whether the player with that position is over this frame.
	/// </summary>
	/// <returns><c>true</c> if the player is over this frame; otherwise, <c>false</c>.</returns>
	/// <param name="pos">Position.</param>
	private bool IsOverMe(Vector3 pos)
	{
		if (pos.x > transform.position.x - maxDistance_x && pos.x < transform.position.x + maxDistance_x && pos.y > transform.position.y - maxDistance_y && pos.y < transform.position.y + maxDistance_y)
			return true;
		return false;
	}


	/// <summary>
	/// Select or deselect the frame. Select means is highlight the frame.
	/// </summary>
	public void Select()
	{
		isSelected = !isSelected;
		string spriteName;
		int offsetSprite;
		Color col;
		if (isSelected == false) {
			spriteName = "rect";
			col = new Color (0, 0, 0, 0.2f);
			offsetSprite = 1;
		} else {
			spriteName = "rect";
			col = new Color (255, 255, 255, 0.2f);
			offsetSprite = 1;
		}

		string filePath = "Sprites/" + spriteName;
		var spr  = Resources.LoadAll(filePath);
		context.sprite = (Sprite)spr[offsetSprite];
		context.color = col;
	}


	/*IEnumerator playSelectSound(){
	 * selecSound.Play();
	 * }
	 * 
	 * IEnumerator playnotSelectSound(){
	 * notSelecSound.Play();
	 * }
		
	} */



	public void turnCard(){
		//card.enabled = false;
		isRounded = true;
		isTurning = true;
	}


	public void coverCard(){
		//card.enabled = true;
		isRounded = false;
		isCovering = true;
	}


	/// <summary>
	/// Finds the closest player based on the position.
	/// </summary>
	/// <returns>The closest.</returns>
	/// <param name="position">Position.</param>
	/// <param name="tag">Tag.</param>
	public GameObject FindClosest(Vector3 position, string tag)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(tag);
		GameObject closest = null;
		float distance = Mathf.Infinity;
		foreach (GameObject go in gos)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

}
