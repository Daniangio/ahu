﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using HueLights;
using System;

public class SceneMasterScrMemo : MonoBehaviour {

	private int totalTurnedElements;
	private int actualTurnedElements_1;
	private int actualTurnedElements_2;
	private int points_1 = 0;
	private int points_2 = 0;
	private int totalPoints; //da mettere in base al livello
	private bool gameStarted_1;
	private bool gameStarted_2;
	private float timer; //time_first_team += Time.deltaTime; in update del gioco griglia
	//private float time_second_team = 0.0f; // solo uno se ti salvi ogni sessione il tempo svolto.
	//public int teamsWhoEnded;

	//private GameObject[] turnedCards;
	//private GameObject[] turnedCards2;

	private int numberOfPlayers;
	public GameObject playerPrefab;
	private GameObject[] players_field_1;
	private GameObject[] players_field_2;
	public LocalKinectManager localKinectManager;
	public RemoteKinectManager remoteKinectManager;


	public FramesOrganizerCone framesOrganizer_1;
	public FramesOrganizerCone framesOrganizer_2;
	public Text textMessage_1;
	public Text textMessage_2;

	private AudioSource audioSource;
	public AudioClip initialAudio;
	public AudioClip finalAudio;

	// Use this for initialization

	void Startup () { 
		audioSource = GetComponent<AudioSource> ();

		numberOfPlayers = 2; //prendi da choice status se quando carichi gioco è il gioco memo-> setta choice status con il giusto numero di giocatori
		totalPoints = 4; // prendi da choicestatus
		totalTurnedElements = 2;
		actualTurnedElements_1 = 0;
		actualTurnedElements_2 = 0;

		HueLightControl c = GameObject.FindGameObjectWithTag ("HueLight").GetComponent<HueLightControl>();
		c.stop();

		//Setup first field
		players_field_1 = new GameObject[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i += 1) {
			GameObject player = Instantiate (playerPrefab, new Vector3 (0, 0), Quaternion.identity);
			player.name = "Player" + i.ToString ();
			players_field_1 [i] = player;
		}

		//Setup second field
		players_field_2 = new GameObject[numberOfPlayers];
		for (int i = numberOfPlayers; i < numberOfPlayers * 2; i += 1) {
			GameObject player = Instantiate (playerPrefab, new Vector3 (0, 0), Quaternion.identity);
			player.name = "Player" + i.ToString ();
			players_field_2 [i-numberOfPlayers] = player;
		}
			

		localKinectManager.SetupPlayers (numberOfPlayers, 1);
		localKinectManager.Initialize ();
		remoteKinectManager.SetupPlayers (numberOfPlayers, 2);
		remoteKinectManager.Initialize ();

		//Initialize first session
		gameStarted_1 = false;
		gameStarted_2 = false;

	}

	public void PlayInitialAudio(AudioClip clip)
	{
		Startup ();
		initialAudio = clip;
		StartCoroutine (InitialAudio ());
	}

	IEnumerator InitialAudio()
	{
		audioSource.clip = initialAudio;
		audioSource.Play();
		yield return new WaitWhile (() => audioSource.isPlaying);

		framesOrganizer_1.InitializeSession ();
		framesOrganizer_2.InitializeSession ();
		//reset timer before start the actual game
		timer = 0.0f;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) { //&& Time.timeScale == 1
			points_1 += 1;
			Debug.Log("punto campo 1");
			LedBarMaster.ShowEffect (2, 1, 5000);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow) ) { //&& Time.timeScale == 1
			points_2 += 1;
			Debug.Log("punto campo 2");
			LedBarMaster.ShowEffect (2, 10, 5000);
		}

		timer += Time.deltaTime;

		if (ChoiceStatus.mode == "Collaborative") {
			if (points_1 == totalPoints && points_2 == totalPoints) {
				
				if (ChoiceStatus.progressionStory == 1) {
					StartCoroutine (End ());
					ChoiceStatus.timeTeam_1 = timer;

				}
				if (ChoiceStatus.progressionStory == 2) {
					ChoiceStatus.timeTeam_2 = timer;
					ChoiceStatus.DecreteFastest ();
					SceneManager.LoadScene ("MemoPositioning");
					//ChoiceStatus.DecreteWinner (); lo fai già in choicestatus

				}
			}
		}

		if (ChoiceStatus.mode == "Competitive") {
			if (points_1 == points_2 && points_1 == totalPoints) {
				ChoiceStatus.teamPoints_1 = 1;
				ChoiceStatus.teamPoints_2 = 1;
				ChoiceStatus.LoadNextGameScene();
			}
			if (points_1 == totalPoints){
				ChoiceStatus.teamPoints_1 = 1;
				ChoiceStatus.teamPoints_2 = 0;
				//ChoiceStatus.DecreteWinner ();

				ChoiceStatus.LoadNextGameScene();
			}
			if (points_2 == totalPoints){
				//setta il secondo come vincitore e chiama new session
				ChoiceStatus.teamPoints_1 = 0;
				ChoiceStatus.teamPoints_2 = 1;
				//ChoiceStatus.DecreteWinner ();
				ChoiceStatus.LoadNextGameScene();
			}

		}

		for (int i = 0; i < numberOfPlayers; i++) {
			
			if (players_field_1 [i].GetComponent<PlayerScr> ().gestureTurned == true){// && players_field_1[i].GetComponent<PlayerScr>().actualFrame != null) {
				//Debug.Log("scenemaster player gestureturned = true");

				//prendi il frame in cui si trova in quel momento il player
				GameObject frame = players_field_1 [i].GetComponent<PlayerScr> ().FindClosest (players_field_1 [i].GetComponent<Transform> ().position, "Frame");
				if (frame.GetComponent<FrameScrMemo> ().isRounded == false) {
					//controlGesture (players_field_1 [i].GetComponent<PlayerScr> ().actualFrame);
					controlGesture (frame.GetComponent<FrameScrMemo> ());
				}
			}


			if (players_field_2 [i].GetComponent<PlayerScr> ().gestureTurned == true) { //&& players_field_2[i].GetComponent<PlayerScr>().actualFrame != null
				//Debug.Log("scenemaster player gestureturned = true");
				GameObject frame = players_field_2 [i].GetComponent<PlayerScr> ().FindClosest (players_field_2 [i].GetComponent<Transform> ().position, "Frame");
				if (frame.GetComponent<FrameScrMemo> ().isRounded == false) {
					//controlGesture (players_field_1 [i].GetComponent<PlayerScr> ().actualFrame);
					controlGesture (frame.GetComponent<FrameScrMemo> ());
				}
			}
		}



//		if (gameStarted_1 == true && actualTurnedElements == totalTurnedElements) {   //forse non serve controllare cosi con update ma solo quando tutte le gesture sono state fatte, quindi metodo a parte
//			textMessage_1.text = "4 girate!";
//
//			//LedBarMaster.AddPoints (1, 1);
//			//LedBarMaster.ShowEffect (1, 3, 10000);
//			//ResetStage (1);
//			//framesOrganizer_1.NewSession ();
//		}
//		if (gameStarted_2 == true && actualTurnedElements == totalTurnedElements) { //actualcorrectsElements = numplayer perchè se metto totalcorrects = 6 ma i giocatori sono 4 non vincono mai
//			textMessage_2.text = "Complimenti! Hai fatto un punto!";
//			//LedBarMaster.AddPoints (1, 1);
//			//LedBarMaster.ShowEffect (2, 3, 10000);
//			//ResetStage (2);
//			//framesOrganizer_2.NewSession ();
//		}
//
////		if (teamsWhoEnded == 2) {
////			StartCoroutine (End ());
////			teamsWhoEnded = 0;
//		}

	}

	IEnumerator End ()
	{
		audioSource.clip = finalAudio;
		audioSource.Play();
		yield return new WaitWhile (() => audioSource.isPlaying);
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene ("MemoPositioning");
	}

	public void PlaySound(AudioClip clip)
	{
		audioSource.clip = clip;
		audioSource.Play();
	}



	/// <summary>
	/// Controls if the gesture can be done based on the frame and if yes it performs it.
	/// </summary>
	/// <param name="frame">Frame</param>
	public void controlGesture(FrameScrMemo frame){ //passa anche player se devi controllare se la puoi fare
		bool correct;
		if (frame.fieldNumber == 1 && frame.sector == "left"){
			if (framesOrganizer_1.isRoundedCardLeft () == false){
				frame.turnCard ();
				Debug.Log("Girato carta 1 left");
				actualTurnedElements_1 += 1;
				checkAction (frame.fieldNumber, frame.id);
			}
		}
		if (frame.fieldNumber == 1 && frame.sector == "right"){
			if (framesOrganizer_1.isRoundedCardRight () == false){
				frame.turnCard ();
				Debug.Log("Girato carta 1 right");
				actualTurnedElements_1 += 1;
				checkAction (frame.fieldNumber, frame.id);
			}
		}
		if (frame.fieldNumber == 2 && frame.sector == "left"){
			if (framesOrganizer_2.isRoundedCardLeft () == false){
				frame.turnCard ();
				Debug.Log("Girato carta 2 left");
				actualTurnedElements_2 += 1;
				checkAction (frame.fieldNumber, frame.id);
			}
		}
		if (frame.fieldNumber == 2 && frame.sector == "right"){
			if (framesOrganizer_2.isRoundedCardRight () == false){
				frame.turnCard ();
				Debug.Log("Girato carta 2 right");
				actualTurnedElements_2 += 1;
				checkAction (frame.fieldNumber, frame.id);
			}
		}

	}
		

	/// <summary>
	/// Checks that rounded cards of the field passed as parameter have all the id passed as parameter.
	/// </summary>
	/// <returns><c>true</c>, if rounded cards in the field have all the same id, <c>false</c> otherwise.</returns>
	/// <param name="field">Field of game.</param>
	/// <param name="id">Identifier of the card.</param>
	private bool checkRoundedCards (int field, int id){  //versione competitiva meglio passare il numero di field e ogni field ha il suo array di carte girate invece di averne uno unico.
		//int typeGame = 1; //1 collaborativo 2 competitivo, in base al choiceStatus
		bool correct = true;
		if (field == 1) {
			if (actualTurnedElements_1 == totalTurnedElements) {
			
				GameObject[] rounded_1 = new GameObject[2];
				rounded_1 = framesOrganizer_1.roundedCards ();

				foreach (GameObject card in rounded_1) { //puoi anche usare for
					if (card.GetComponent <FrameScrMemo> ().id != id) {
						correct = false;
					}
				}

			}
		}

		if (field == 2) { 
			if (actualTurnedElements_2 == totalTurnedElements) { 
			
				GameObject[] rounded_2 = new GameObject[2];
				rounded_2 = framesOrganizer_2.roundedCards ();

				foreach (GameObject card in rounded_2) { //puoi anche usare for
					if (card.GetComponent <FrameScrMemo> ().id != id) {
						correct = false;
					}
				}
			}
		}
		return correct;
	}

	IEnumerator CoverCards (){
		yield return new WaitForSeconds (4f);
		GameObject[] rounded_1 = new GameObject[2];
		rounded_1 = framesOrganizer_1.roundedCards ();
		GameObject[] rounded_2 = new GameObject[2];
		rounded_2 = framesOrganizer_2.roundedCards ();
		rounded_1 [0].GetComponent <FrameScrMemo> ().coverCard ();
		rounded_1 [1].GetComponent <FrameScrMemo> ().coverCard ();
		rounded_2 [0].GetComponent <FrameScrMemo> ().coverCard ();
		rounded_2 [1].GetComponent <FrameScrMemo> ().coverCard ();
	}

	/// <summary>
	/// Check if the action can be performed an if yes it performs it.
	/// </summary>
	/// <param name="field">Field.</param>
	/// <param name="id">Identifier.</param>
	private void checkAction (int field, int id){  //versione competitiva meglio passare il numero di field e ogni field ha il suo array di carte girate invece di averne uno unico.

		if (ChoiceStatus.mode == "Collaborative") {

			if (actualTurnedElements_1 == totalTurnedElements && actualTurnedElements_2 == totalTurnedElements) {
				//ci sono 4 carte girate
				if (checkRoundedCards(1,id) && checkRoundedCards (2,id)){
					//corretti tutti e 4, distruggo carte da gioco
					framesOrganizer_1.destroyCardsRounded ();
					framesOrganizer_2.destroyCardsRounded ();
					points_1 += 1;
					points_2 += 1;
				}else{
						//sono sbagliate aspetta che si giri ultima carta e poi coprile
						StartCoroutine (CoverCards ());  //qui dovresti inserire effetto

				}
				actualTurnedElements_1 = 0;
				actualTurnedElements_2 = 0;
			}

		}

		if(ChoiceStatus.mode == "Competitive"){
			
			if (field == 1 && actualTurnedElements_1 == totalTurnedElements) {
				if (checkRoundedCards(1,id)){
					//le due carte campo 1 sono giuste
					framesOrganizer_1.destroyCardsRounded ();
					points_1 += 1;
				}else{
					//sono sbagliate
					GameObject[] rounded_1 = new GameObject[2];
					rounded_1 = framesOrganizer_1.roundedCards ();
					rounded_1 [0].GetComponent <FrameScrMemo> ().coverCard ();
					rounded_1 [1].GetComponent <FrameScrMemo> ().coverCard ();
					actualTurnedElements_1 = 0;
				}
			}

			if (field == 2 && actualTurnedElements_2 == totalTurnedElements) {
				if (checkRoundedCards(2,id)){
					//le due carte campo 2 sono giuste
					framesOrganizer_2.destroyCardsRounded ();
					points_2 += 1;
				}else{
					//sono sbagliate
					GameObject[] rounded_2 = new GameObject[2];
					rounded_2 = framesOrganizer_2.roundedCards ();
					rounded_2 [0].GetComponent <FrameScrMemo> ().coverCard ();
					rounded_2 [1].GetComponent <FrameScrMemo> ().coverCard ();
					actualTurnedElements_2 = 0;
				}
			}

		}
		
	}




}


