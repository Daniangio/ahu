﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Iterator<T> {

	private List<T> myList = new List<T>();
	private int head = 0;

	public void AddToPool (T element)
	{
		myList.Add (element);
	}

	public void AddToPool (T[] elements)
	{
		foreach (T elem in elements)
			myList.Add (elem);
	}
	
	public T Next()
	{
		T elem = myList [head];
		head += 1;
		return elem;
	}

	public T Current()
	{
		if (head > 0)
			head -= 1;
		T elem = myList [head];
		head += 1;
		return elem;
	}

	public bool IsEmpty()
	{
		return (head == myList.Count);
	}

}