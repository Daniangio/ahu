﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

namespace HueLights
{
	public class CicleEffectWorker : MonoBehaviour, LightEffect
	{	
		/// <summary>
		/// Generate a cicle effect on a color.
		/// Red, green or blue
		/// </summary>
		/// <returns></returns>
		/// <param name="light">Light on which you wanna apply the effect</param>
		/// <param name="control">Light controller</param>
		/// <param name="type">Color of the cicle: red for fire effect, blue or green. The default is red.</param>
		public IEnumerator DoWork(HueLight light,HueLightController control, string type = "red")
		{	
			switch (type) {
			case "red":
				yield return DoFire (light, control);
				break;
			case "blue":
				yield return DoBlue (light, control);
				break;
			case "green":
				yield return DoGreen (light, control);
				break;
			case "rainbow":
				yield return DoRainbow (light, control);
				break;
			
			}
		}

		public IEnumerator DoFire(HueLight light, HueLightController control)
		{
			while(true){
				int timeRandom = Random.Range (1000,1500);
				int brightness = Random.Range (150, 256);
				string green = ColorShadesGenerator.generateColorBetween(0,200);
				string color = "FF" + green + "00";
				light.setColor (color);
				light.setBrightness (brightness.ToString());
				control.uploadLight (light);
				yield return new WaitForSeconds (timeRandom/1000);
			}
		}

		public IEnumerator DoBlue(HueLight light, HueLightController control)
		{
			while(true){
				int timeRandom = Random.Range (1000,1500);
				int brightness = Random.Range (150, 256);
				string green = ColorShadesGenerator.generateColorBetween(0,225);
				string color = "00" + green + "FF";
				light.setColor (color);
				light.setBrightness (brightness.ToString());
				control.uploadLight (light);
				yield return new WaitForSeconds (timeRandom/1000);
			}
		}

		public IEnumerator DoGreen(HueLight light, HueLightController control)
		{
			while(true){
				int timeRandom = Random.Range (1000,1500);
				int brightness = Random.Range (150, 256);
				string red = ColorShadesGenerator.generateColorBetween(0,200);
				string color = red + "FF" + "00";
				light.setColor (color);
				light.setBrightness (brightness.ToString());
				control.uploadLight (light);
				yield return new WaitForSeconds (timeRandom/1000);
			}
		}

		public IEnumerator DoRainbow(HueLight light, HueLightController control)
		{
			while(true){
				int maxRed = 200;
				int maxGreen = 200;
				int maxBlue = 200;
				int timeRandom = Random.Range (2000,3000);
				int brightness = Random.Range (150, 256);
				string red = ColorShadesGenerator.generateColorBetween(1,maxRed);
				string green = ColorShadesGenerator.generateColorBetween(1,maxGreen);
				string blue = ColorShadesGenerator.generateColorBetween(1,maxBlue);
				string color = red + green + blue;
				light.setColor (color);
				light.setBrightness (brightness.ToString());
				control.uploadLight (light);
				yield return new WaitForSeconds (timeRandom/1000);
			}
		}

	}
}

