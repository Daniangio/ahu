﻿using System;

namespace HueLights
{
	[Serializable]
	public class HueLight
	{
		public string Action;

		public string Color;

		public string Brightness;

		public string idHue;


		public HueLight (string id)
		{
			this.Action = "LightCommand";
			this.Color = "#000000";
			this.Brightness = "255";
			this.idHue = id;
		}


		/// <summary>
		/// Sets the color of the Hue.
		/// </summary>
		/// <param name="color">Color to set</param>
		public void setColor(string color)
		{
			string str = "#" + color;
			this.Color = str;
		}

		public void setBrightness(string brightness)
		{
			this.Brightness = brightness;
		}

		public void turnOff()
		{
			this.setColor ("000000");
		}

	}
}

