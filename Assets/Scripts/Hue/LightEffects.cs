﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HueLights
{
	public class LightEffects : MonoBehaviour
	{
		List<IEnumerator> badGuys = new List<IEnumerator>(); 
		/// <summary>
		/// Sets the color of a group of lights.
		/// </summary>
		/// <param name="lights">Lights.</param>
		/// <param name="controller">Controller.</param>
		/// <param name="color">Color.</param>
		/// <param name="brightness">Brightness.</param>
		public void setGroupColor(HueLight[] lights, HueLightController controller, string color,string brightness){
			foreach (HueLight l in lights) {
				l.setColor (color);
				l.setBrightness (brightness);
				controller.uploadLight (l);
			}
		}

		/// <summary>
		/// Sets the color of a group of lights.
		/// </summary>
		/// <param name="lights">Lights.</param>
		/// <param name="controller">Controller.</param>
		/// <param name="color">Color.</param>
		public void setGroupColor(HueLight[] lights, HueLightController controller, string color){
			foreach (HueLight l in lights) {
				l.setColor (color);
				controller.uploadLight (l);
			}
		}

		/// <summary>
		/// Sets the group brightness.
		/// </summary>
		/// <param name="lights">Lights.</param>
		/// <param name="controller">Controller.</param>
		/// <param name="brightness">Brightness.</param>
		public void setGroupBrightness(HueLight[] lights, HueLightController controller, string brightness){
			foreach (HueLight l in lights) {
				l.setBrightness (brightness);
				controller.uploadLight (l);
			}
		}

		/// <summary>
		/// Does one effect on a group of lights
		/// </summary>
		/// <returns></returns>
		/// <param name="lights">Lights on which you apply the effect</param>
		/// <param name="controller">Controller of the lights</param>
		/// <param name="effectWorker">The Effect to apply</param>
		/// <param name="seconds">Duration of the effect if you use a negative number the effect is an infinite loop</param>
		/// <param name="opt">Optional string for option of the effects</param>
		public IEnumerator DoEffect(HueLight[] lights, HueLightController controller,LightEffect effectWorker, int seconds, string opt = null)
		{
			setGroupColor (lights, controller, "FFFFFF","255");
			foreach (HueLight l in lights) {
				IEnumerator i = effectWorker.DoWork (l, controller, opt);
				badGuys.Add (i);
				StartCoroutine (i);
			}
			if (seconds > 0) {
				yield return new WaitForSeconds (seconds);
				if (badGuys.Count != 0) {
					foreach (IEnumerator guy in badGuys) {
						StopCoroutine (guy);
					}
					badGuys.Clear ();
				}
			}

		}
		/// <summary>
		/// Stops all the effects.
		/// </summary>
		public void stopEffects()
		{	if (badGuys.Count != 0) {
				foreach (IEnumerator guy in badGuys) {
					StopCoroutine (guy);
				}
				badGuys.Clear ();
			}
		}
	}
}