﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.SceneManagement;

namespace HueLights
{
public class HueLightControl : MonoBehaviour {

		HueLightController h;
		HueLight[] lights;
		LightEffects effect;

		void Awake() {
			DontDestroyOnLoad (transform.gameObject);
			h = GetComponent<HueLightController> ();
			h.SetIp ("http://192.168.0.3:7070");
			HueLight light1 = new HueLight ("1");
			HueLight light2 = new HueLight ("2");
			HueLight light3 = new HueLight ("8");
			lights = new HueLight[3];
			lights [0] = light1;
			lights [1] = light2;
			lights [2] = light3;
			effect = GetComponent<LightEffects> ();
		}

		// Use this for initialization
		void Start () {
			PlayEffect (10, "rainbow");
		}

		void OnLevelWasLoaded() {
			string name = SceneManager.GetActiveScene().name;
			if (name == "MenuIniziale") {
				effect.stopEffects ();
				PlayEffect (-1, "red");
			} else if (name == "Learning_1" || name == "CollaborativoLuci_1") {
				effect.stopEffects ();
			} else if (name == "Reward") {
				PlayEffect (30, "rainbow");
			}
		}

		public void stop()
		{
			effect.stopEffects ();
		}

		public void PlayEffect(int duration, string effectName)
		{
			StartCoroutine (effect.DoEffect (lights, h, GetComponent<CicleEffectWorker>(), duration, effectName));
		}

	}
}