﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

namespace HueLights
{
	public class HueLightController : MonoBehaviour
	{

		private string _url = "";


		private IEnumerator SendLight(HueLight light) {
			string json = JsonUtility.ToJson(light);
			string request = this._url;
			UnityWebRequest www = new UnityWebRequest(request, "POST");
			byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
			www.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
			www.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
			www.SetRequestHeader("Content-Type", "application/json");
			yield return www.SendWebRequest ();
		}

		/// <summary>
		/// Sets the ip of the philips hue server.
		/// </summary>
		/// <param name="ip">Ip of the server</param>
		public void SetIp(string ip)
		{
			this._url = ip;
		}

		/// <summary>
		/// Uploads the light.
		/// </summary>
		/// <param name="light">Light to upload</param>
		public void uploadLight(HueLight light)
		{
			StartCoroutine (SendLight (light));
		}
	}
}