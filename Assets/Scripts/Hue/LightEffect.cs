﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace HueLights
{
	/// <summary>
	/// Light effect interface.
	/// </summary>
	public interface LightEffect
	{
		IEnumerator DoWork(HueLight light, HueLightController control, string type = "red");
	}
}

