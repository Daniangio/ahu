﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HueLights
{
	public static class ColorShadesGenerator
	{
		/// <summary>
		/// Generates one color between minCol, maxCol and return his string in hex.
		/// </summary>
		/// <returns>The color randomly generated in hex string<see cref="System.String"/>.</returns>
		/// <param name="minCol">Minimum color</param>
		/// <param name="maxCol">Maximum color</param>
		public static string generateColorBetween(int minCol, int maxCol)
		{
			int A = Random.Range(minCol, maxCol);
			string color = A.ToString("X");
			if (color.Length == 1) {
				color = "0" + color;
			}
			return color;
		}
	}
}

