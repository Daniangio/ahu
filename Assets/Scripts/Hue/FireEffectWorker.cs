﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

namespace HueLights
{
	public class FireEffectWorker : MonoBehaviour
	{	

		public IEnumerator DoWork(HueLight[] lights,HueLightController control)
		{
			while(true){
				int nLights = lights.Length;
				int idL = Random.Range (0, nLights);
				HueLight light = lights [idL];
				int timeRandom = Random.Range (1,3);
				int A = Random.Range(0, 200);
				int brightness = Random.Range (150, 256);
				string green = A.ToString("X");
				string color = "FF" + green + "00";
				light.setColor (color);
				light.setBrightness (brightness.ToString());
				control.uploadLight (light);
				yield return new WaitForSeconds (timeRandom);
			}

		}
	}
}

