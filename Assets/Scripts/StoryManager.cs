﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryManager : MonoBehaviour {

	int progressionStory;
	public VoiceManager audioManager;
	public SpriteRenderer back_1;
	public SpriteRenderer back_2;

	public Sprite[] globalBackImages; //Tutte le immagini di background della storia
	public float[] globalSecondsPerBack; //Elenco dei tempi di attesa prima di cambiare background, metti 0 se il background non cambia

	private int initialIndex; //Indice da cui iniziare a selezionare i background per la scena specifica (es. backround da 3 a 5)
	private int finalIndex;
	private Sprite[] actualBackImages;
	private float[] actualSecondsPerBack;

	// Use this for initialization
	void Start () {
		progressionStory = ChoiceStatus.getProgressionStory ();
		SelectDataForThisScene ();
		StartCoroutine (BackgroundSlideshow ());
		audioManager.StartAudio (progressionStory);
		//LedBarMaster.Initialize (10);
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Return))
			ChoiceStatus.LoadNextGameScene ();
	}

	private void SelectDataForThisScene ()
	{
		switch (progressionStory) {
		case 0:
			initialIndex = 0;
			finalIndex = 1;
			FeedData ();
			break;
		case 1:
			initialIndex = 2;
			finalIndex = 2;
			FeedData ();
			break;
		case 2:
			initialIndex = 3;
			finalIndex = 3;
			FeedData ();
			break;
		case 3:
			initialIndex = 4;
			finalIndex = 4;
			FeedData ();
			break;
		case 4:
			initialIndex = 5;
			finalIndex = 5;
			FeedData ();
			break;
		case 5:
			initialIndex = 6;
			finalIndex = 6;
			FeedData ();
			break;
		case 6:
			initialIndex = 7;
			finalIndex = 7;
			FeedData ();
			break;
		case 7:
			initialIndex = 8;
			finalIndex = 8;
			FeedData ();
			break;
		case 8:
			initialIndex = 9;
			finalIndex = 9;
			FeedData ();
			break;
		case 9:
			initialIndex = 10;
			finalIndex = 10;
			FeedData ();
			break;
		}
	}

	private void FeedData ()
	{
		int count = 0;
		actualBackImages = new Sprite[(finalIndex - initialIndex + 1)];
		for (int i = initialIndex; i <= finalIndex; i += 1) {
			actualBackImages [count] = globalBackImages [i];
			count += 1;
		}

		count = 0;
		actualSecondsPerBack = new float[(finalIndex - initialIndex + 1)];
		for (int i = initialIndex; i <= finalIndex; i += 1) {
			actualSecondsPerBack [count] = globalSecondsPerBack [i];
			count += 1;
		}
	}

	IEnumerator BackgroundSlideshow()
	{
		for (int i = 0; i <= (finalIndex - initialIndex); i += 1) {
			var width = actualBackImages [i].bounds.size.x;
			var height = actualBackImages [i].bounds.size.y;
			var worldScreenHeight = Camera.allCameras [0].orthographicSize * 2.0;
			var worldsScreenWidth = worldScreenHeight / Screen.height * Screen.width;
			back_1.sprite = actualBackImages [i];
			back_2.sprite = actualBackImages [i];
			back_1.transform.localScale = new Vector3 ((float)worldsScreenWidth / width, (float)worldScreenHeight / height);
			back_2.transform.localScale = new Vector3 ((float)worldsScreenWidth / width, (float)worldScreenHeight / height);
			yield return new WaitForSeconds (actualSecondsPerBack[i]);
		}
	}

	public void NextScene ()
	{
		StartCoroutine (waiter());
	}

	IEnumerator waiter()
	{
		yield return new WaitForSeconds (1.5f);
		ChoiceStatus.LoadNextGameScene ();
	}

}