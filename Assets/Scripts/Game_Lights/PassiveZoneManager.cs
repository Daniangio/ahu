﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HueLights;
using System.IO;
using System;

public class PassiveZoneManager : MonoBehaviour {

	public ActiveZoneManager activeZoneManager;
	public Text textMessage;

	private Iterator<string> dataPool;
	private string[] dataPoolFiles;
	private string gameDataFileName;
	public GameObject elementPrefab;
	private GameObject[] elementsDisposed;
	private FrameData[] frames;

	void Start () {
		dataPool = new Iterator<string> ();

		dataPoolFiles = new string[4];
		dataPoolFiles [0] = "Collaborative_Lights_Passive_1.json";
		dataPoolFiles [1] = "Collaborative_Lights_Passive_2.json";
		dataPoolFiles [2] = "Collaborative_Lights_Passive_3.json";
		dataPoolFiles [3] = "Collaborative_Lights_Passive_4.json";
		FillDataPool (dataPoolFiles);
	}

	private void Dispose()
	{
		elementsDisposed = new GameObject[2];
		for (int i = 0; i < 2; i += 1)
		{
			GameObject elemGO = GameObject.Instantiate (elementPrefab, new Vector3 (
				Camera.allCameras[1].transform.position.x + (i * 2 - 1) * 0.6f, 0, 0), Quaternion.identity);
			elemGO.transform.localScale = new Vector3 (0.4f, 0.4f, 1);
			elementsDisposed [i] = elemGO;
			SpriteRenderer sr = elemGO.GetComponent<SpriteRenderer> ();
			bool dueCifre = (frames[i].spriteName[frames[i].spriteName.Length-2] != '_');
			int index = dueCifre ? ((int)Char.GetNumericValue(frames[i].spriteName[frames[i].spriteName.Length-1]) + 1 + 10 * ((int)Char.GetNumericValue(frames[i].spriteName[frames[i].spriteName.Length-2])))
				:
				(int)Char.GetNumericValue(frames[i].spriteName[frames[i].spriteName.Length-1]) + 1;
			frames[i].spriteName = dueCifre ? frames[i].spriteName.Substring(0,frames[i].spriteName.Length - 3) : frames[i].spriteName.Substring(0,frames[i].spriteName.Length - 2);
			var filePath = "Sprites/" + frames[i].spriteName;
			var spr = Resources.LoadAll(filePath);
			Sprite sprite = (Sprite)spr[index];
			sr.sprite = sprite;
		}

	}

	public void FillDataPool(string[] dataFileNames)
	{
		dataPool.AddToPool (dataFileNames);
	}

	private void LoadGameData()
	{
		gameDataFileName = dataPool.Next ();

		string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}

	private void PanicLoadGameData()
	{
		gameDataFileName = dataPool.Current();

		string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}

	public void ClearField()
	{
		foreach (GameObject go in elementsDisposed)
			Destroy(go);
	}

	public void NextRound ()
	{
		if (elementsDisposed != null)
			ClearField ();
		if (dataPool.IsEmpty ()) {
			return;
		}
		LoadGameData ();
		Dispose ();
	}

	public void PanicNextRound ()
	{
		if (elementsDisposed != null)
			ClearField ();
		if (dataPool.IsEmpty ()) {
			return;
		}
		PanicLoadGameData ();
		Dispose ();
	}

}
