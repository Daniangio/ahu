﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour {

	private ActiveZoneManager activeZoneManager;
	public SpriteRenderer spriteRenderer;
	private string filePath;
	private string filePathSelected;

	private bool isSelected = false;
	public int teamNumber;
	public bool isCorrect;
	public string color;

	private int i; //Used to move the frame into the correct sector of the screen
	private int j;

	private float maxDistance_x;
	private float maxDistance_y;

	//public AudioClip selecSound;
	//public AudioClip notSelecSound;

	void Start() {

	}

	public void Initialize(string framesOrganizerName, string spriteName, bool isCorrect, string color, int i, int j)
	{
		activeZoneManager = GameObject.Find ("ActiveZoneManager").GetComponent<ActiveZoneManager> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();

		//Load sprite
		filePath = "Sprites/" + "rocciaGiocoHue";
		filePathSelected = "Sprites/" + "rocciaGiocoHue";
		var spr = Resources.LoadAll(filePath);
		Sprite sprite = (Sprite)spr[1];
		spriteRenderer.sprite = sprite;
		if (spriteRenderer.sprite == null)
			Debug.Log ("Sprite non caricata");

		this.isCorrect = isCorrect;
		this.isSelected = false;
		this.color = color;

		this.i = i;
		this.j = j;
		this.teamNumber = 1;


	}

	void Update()
	{
		if (teamNumber > 0) {
			GameObject nearestPlayer = FindClosest (transform.position, "Player");
			if (IsOverMe (nearestPlayer.transform.position) && !isSelected) {
				Select (nearestPlayer);
			}
			if (!IsOverMe (nearestPlayer.transform.position) && isSelected) {
				Select (nearestPlayer);
			}
		}
		ResizeSpriteToScreen (spriteRenderer, 0.9f);
	}

	private void ResizeSpriteToScreen(SpriteRenderer sr, float size) {
		if (sr == null)
			return;

		var worldScreenHeight = Camera.main.orthographicSize * 2;
		var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		Vector2 local_sprite_size = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

		float frame_width = (worldScreenWidth - activeZoneManager.border_left - activeZoneManager.border_right) / activeZoneManager.horizontalFrames;
		float frame_height = (worldScreenHeight - activeZoneManager.border_top) / activeZoneManager.verticalFrames;
		float myScale_y = (1 / local_sprite_size.y) * frame_height * size;
		float myScale_x = (1 / local_sprite_size.x) * frame_width * size;
		sr.transform.localScale = new Vector3 (myScale_x, myScale_y, 1);

		float offset_x = frame_width + Camera.allCameras[teamNumber - 1].transform.position.x + activeZoneManager.border_right - activeZoneManager.border_left;
		float offset_y = frame_height + Camera.allCameras[teamNumber - 1].transform.position.y - activeZoneManager.border_top / 2;

		transform.position = new Vector3 (i * frame_width + offset_x, j * frame_height + offset_y, 0);

		maxDistance_x = local_sprite_size.x * myScale_x / 2;
		maxDistance_y = local_sprite_size.y * myScale_y / 2;
	}

	private bool IsOverMe(Vector3 pos)
	{
		if (pos.x > transform.position.x - maxDistance_x && pos.x < transform.position.x + maxDistance_x && pos.y > transform.position.y - maxDistance_y && pos.y < transform.position.y + maxDistance_y)
			return true;
		return false;
	}

	public void Select(GameObject player)
	{
		isSelected = !isSelected;
		string whichPath;
		int offsetSprite;
		if (isSelected == false) {
			whichPath = filePath;
			//play sound notSelected
			//notSelecSound.Play();
			offsetSprite = 1;
		} else {
			whichPath = filePathSelected;
			if (!activeZoneManager.preGameModeOn)
				activeZoneManager.SetLightColor (player, color);
			//play sound selected
			//selecSound.Play();
			offsetSprite = 1;
		}
		if (isSelected == true && isCorrect == true)
			activeZoneManager.AddCorrect (1);
		else if (isSelected == false && isCorrect == true)
			activeZoneManager.RemoveCorrect (1);
		else if (isSelected == true && isCorrect == false)
			activeZoneManager.RemoveCorrect (1);
		else if (isSelected == false && isCorrect == false)
			activeZoneManager.AddCorrect (1);

		if (!activeZoneManager.preGameModeOn) {
			var spr = Resources.LoadAll (whichPath);
			if (spr != null)
				spriteRenderer.sprite = (Sprite)spr [offsetSprite];
		}
	}


	/*IEnumerator playSelectSound(){
	 * selecSound.Play();
	 * }
	 * 
	 * IEnumerator playnotSelectSound(){
	 * notSelecSound.Play();
	 * }
		
	} */

	public GameObject FindClosest(Vector3 position, string tag)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(tag);
		GameObject closest = null;
		float distance = Mathf.Infinity;
		foreach (GameObject go in gos)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

	public void AnimateIfCorrect ()
	{
		if (isCorrect)
			StartCoroutine(Animate ());
	}

	public IEnumerator Animate ()
	{
		for (int i = 1; i < 26; i += 1) {
			spriteRenderer.color = new Color (0, 1, 0, (float)(i) / 25);
			yield return new WaitForSeconds (0.02f);
		}
		for (int i = 26; i > 1; i -= 1) {
			spriteRenderer.color = new Color (0, 1, 0, (float)(i) / 25);
			yield return new WaitForSeconds (0.02f);
		}
		for (int i = 1; i < 26; i += 1) {
			spriteRenderer.color = new Color (0, 1, 0, (float)(i) / 25);
			yield return new WaitForSeconds (0.02f);
		}
	}

}