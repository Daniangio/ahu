﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using HueLights;

public class ActiveZoneManager : MonoBehaviour {

	public PassiveZoneManager passiveZoneManager;
	public LocalKinectManager kinectManager;
	public Text textMessage;
	public GameObject playerPrefab;
	public GameObject zonePrefab;
	public HueLightController hueController;
	private HueLight hueLight_1;
	private HueLight hueLight_2;

	private AudioSource audioSource;
	private AudioClip audioClip;
	public AudioClip scorePointAudio;
	public AudioClip calibrationAudio;
	public AudioClip finalAudio;
	private GameObject[] players;
	private int numberOfPlayers = 2;
	public int horizontalFrames = 3;
	public int verticalFrames = 3;
	private GameObject[] framesDisposed;
	private FrameData[] frames;
	private Iterator<string> dataPool;
	private string[] dataPoolFiles;
	private string gameDataFileName;
	public bool preGameModeOn;
	public bool gameModeOn;
	private bool panic;
	private string color;
	private HueLight[] playerLightBindings; //la i-esima posizione contiene l'id della luce

	private int actualCorrectElements = 0;
	private int globalCorrectElements = 0;

	public float border_left;
	public float border_right;
	public float border_top;

	void Start () {
		audioSource = GetComponent<AudioSource> ();
		dataPool = new Iterator<string> ();

		players = new GameObject[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i += 1) {
			GameObject player = Instantiate (playerPrefab, new Vector3 (0, 0), Quaternion.identity);
			player.name = "Player" + i.ToString ();
			players [i] = player;
		}

		kinectManager.SetupPlayers (numberOfPlayers, 1);
		kinectManager.Initialize ();

		dataPoolFiles = new string[5];
		dataPoolFiles [0] = "Collaborative_Lights_PreGame.json";
		dataPoolFiles [1] = "Collaborative_Lights_1.json";
		dataPoolFiles [2] = "Collaborative_Lights_2.json";
		dataPoolFiles [3] = "Collaborative_Lights_3.json";
		dataPoolFiles [4] = "Collaborative_Lights_4.json";
		FillDataPool (dataPoolFiles);
		LoadGameData ();
		preGameModeOn = true;
		panic = false;
		gameModeOn = false;
		hueController.SetIp ("http://192.168.0.3:7070");
		hueLight_1 = new HueLight ("1");
		hueLight_2 = new HueLight ("2");
		StartCoroutine (LaunchPreGame ());
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow) && Time.timeScale == 1) {
			gameModeOn = false;
			if (preGameModeOn) {
				preGameModeOn = false;
				BindBodiesToLights ();
				if (panic)
					passiveZoneManager.PanicNextRound ();
				else
					passiveZoneManager.NextRound ();
				NextRound ();
			} else {
				StartCoroutine (PrepareForNextRound());
			}
		}
		if (Input.GetKeyDown (KeyCode.RightArrow) && Time.timeScale == 1) {
			Recalibrate ();
		}

		if (gameModeOn && actualCorrectElements == globalCorrectElements) {
			gameModeOn = false;
			if (preGameModeOn) {
				preGameModeOn = false;
				BindBodiesToLights ();
				if (panic)
					passiveZoneManager.PanicNextRound ();
				else
					passiveZoneManager.NextRound ();
				NextRound ();
			} else {
				StartCoroutine (PrepareForNextRound());
			}
		}
		int num = kinectManager.GetNumberOfTrackedBodies ();
		if (gameModeOn == true && preGameModeOn == false && num < 1) {
			Recalibrate ();
		}
	}

	public void Recalibrate()
	{
		StartCoroutine(PlayAudio (calibrationAudio));
		hueLight_1.setColor ("FF0000");
		hueLight_2.setColor ("0000FF");
		hueController.uploadLight (hueLight_1);
		hueController.uploadLight (hueLight_2);
		passiveZoneManager.ClearField ();
		ClearField ();
		PanicLoadGameData ();
		preGameModeOn = true;
		panic = true;
		Dispose ();
		ResetPositions ();
		gameModeOn = true;
	}

	private void Dispose()
	{
		framesDisposed = new GameObject[horizontalFrames * verticalFrames];
		int count = 0;
		for (int i = -(horizontalFrames + 1) / 2; i < horizontalFrames / 2; i += 1)
		{
			for (int j = -(verticalFrames + 1) / 2; j < verticalFrames / 2; j += 1)
			{
				GameObject zoneGO = GameObject.Instantiate (zonePrefab, new Vector3 (0, 0, 0), Quaternion.identity);
				Zone zone = zoneGO.GetComponent<Zone>();
				zone.Initialize(this.name, frames[count].spriteName, frames[count].isCorrect, frames[count].color, i, j);
				framesDisposed [count] = zoneGO;

				if (preGameModeOn && ((i == -2 && j == -1) || (i == 0 && j == -1))) {
					/*var filePath = "Sprites/rect";
					var spr = Resources.LoadAll(filePath);
					Sprite sprite = (Sprite)spr[1];
					zone.spriteRenderer.sprite = sprite;*/
					if (i == -2 && j == -1)
						zone.spriteRenderer.color = Color.red;
					else
						zone.spriteRenderer.color = Color.blue;
				}
				count += 1;
			}
		}
		ResetPositions ();
	}

	public void FillDataPool(string[] dataFileNames)
	{
		dataPool.AddToPool (dataFileNames);
	}

	private void LoadGameData()
	{
		if (panic) {
			panic = false;
			gameDataFileName = dataPool.Current ();
		} else {
			gameDataFileName = dataPool.Next ();
		}

		string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
			string audioPath = Path.Combine ("AudioRecords", loadedData.audioName);
			AudioClip clip = Resources.Load<AudioClip> (audioPath);
			if (clip != null) {
				audioClip = clip;
			}
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}

	private void PanicLoadGameData()
	{
		gameDataFileName = dataPoolFiles [0];

		string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
			string audioPath = Path.Combine ("AudioRecords", loadedData.audioName);
			AudioClip clip = Resources.Load<AudioClip> (audioPath);
			if (clip != null) {
				audioClip = clip;
			}
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}

	public IEnumerator LaunchPreGame()
	{
		HueLightControl c = GameObject.FindGameObjectWithTag ("HueLight").GetComponent<HueLightControl>();
		c.stop();
		hueLight_1.setColor ("FF0000");
		hueLight_2.setColor ("0000FF");
		hueController.uploadLight (hueLight_1);
		hueController.uploadLight (hueLight_2);
		StartCoroutine (PlayAudio (audioClip));
		yield return new WaitWhile (() => audioSource.isPlaying);
		Dispose ();
		StartCoroutine (PlayAudio (calibrationAudio));
		gameModeOn = true;
	}

	public IEnumerator PlayAudio(AudioClip clip)
	{
		audioSource.clip = clip;
		audioSource.Play();
		yield return new WaitWhile (() => audioSource.isPlaying); 
	}

	public IEnumerator FinalAudio()
	{
		StartCoroutine(PlayAudio(finalAudio));
		yield return new WaitWhile (() => audioSource.isPlaying); 
		SceneManager.LoadScene ("Reward");
	}

	public IEnumerator PrepareForNextRound()
	{
		Score ();
		audioSource.clip = scorePointAudio;
		audioSource.Play();
		yield return new WaitWhile (() => audioSource.isPlaying); 
		passiveZoneManager.NextRound ();
		NextRound ();
	}

	public void AddCorrect (int points = 1)
	{
			actualCorrectElements += points;
	}

	public void RemoveCorrect (int points = 1)
	{
		actualCorrectElements -= points;
	}

	public void SetCorrectPositions(int[] correctPositions)
	{
		int j = 0;
		for (int i = 0; i < framesDisposed.Length; i += 1) {
			framesDisposed [i].GetComponent<Zone> ().isCorrect = (j < correctPositions.Length && i == correctPositions [j]) ? true : false;
			if (j < correctPositions.Length && i == correctPositions [j])
				j += 1;
		}
		actualCorrectElements = 0;
		globalCorrectElements = correctPositions.Length;
	}

	private void ResetPositions()
	{
		actualCorrectElements = 0;
		int j = 0;
		for (int i = 0; i < framesDisposed.Length; i += 1) {
			if (framesDisposed [i].GetComponent<Zone> ().isCorrect)
				j += 1;
		}
		globalCorrectElements = j;
	}

	private void BindBodiesToLights ()
	{
		playerLightBindings = new HueLight[2];
		GameObject player = framesDisposed[1].GetComponent<Zone>().FindClosest (framesDisposed[1].transform.position, "Player");
		int index = int.Parse(player.name.Substring(player.name.Length - 1));
		playerLightBindings [index] = hueLight_1;
		player = framesDisposed[7].GetComponent<Zone>().FindClosest (framesDisposed[7].transform.position, "Player");
		index = int.Parse(player.name.Substring(player.name.Length - 1));
		playerLightBindings [index] = hueLight_2;
	}

	public void Score ()
	{
		LedBarMaster.ShowEffect (3, 9, 10000);
		LedBarMaster.AddPoints (1, 1);
		foreach (GameObject g in framesDisposed) {
			g.GetComponent<Zone> ().AnimateIfCorrect ();
		}
	}

	private void NextRound ()
	{
		ClearField ();
		if (dataPool.IsEmpty ()) {
			StartCoroutine (FinalAudio ());
			return;
		}
		LoadGameData ();
		Dispose ();
		ResetPositions ();
		gameModeOn = true;
	}

	private void ClearField()
	{
		foreach (GameObject go in framesDisposed)
			Destroy(go);
	}

	public void SetLightColor(GameObject player, string color)
	{
		int lightId = int.Parse(player.name.Substring(player.name.Length - 1));
		if (lightId == 0) {
			hueLight_1.setColor (color);
			hueController.uploadLight (hueLight_1);
		} else if (lightId == 1) {
			hueLight_2.setColor (color);
			hueController.uploadLight (hueLight_2);
		}
	}
}