﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine.Networking;

namespace KinectServerListener
{
	public class KinectListener_Remote : MonoBehaviour {

		private HttpListener _listener;
		Skeleton[] skeletons;
		const string _serverKinect = "http://192.168.0.3:7080";
		const string myIp = "http://192.168.0.2:7091";
		//Set frequency based on unity fps
		string _configJson = "{ \"type\" : \"KinectCommand\", \"command\" : \"SetUpKinect\", \"frequency\" : 10, \"window\" : 1, \"listeningaddress\" : \"" + myIp + "\"}";
		string _startJson = "{ \"type\" : \"KinectCommand\", \"command\" : \"StartKinect\", \"option\" : \"Streaming\" }";
		string _stopJson = "{ \"type\" : \"KinectCommand\", \"command\" : \"StopKinect\" }";

		// Use this for initialization
		void Start () 
		{
			StartCoroutine (KinectServerSender (_configJson));
			StartCoroutine (KinectServerSender (_startJson));
			_listener = new HttpListener();
			string myPort = myIp.Substring (myIp.Length - 4);
			_listener.Prefixes.Add ("http://*:" + myPort +"/");

			_listener.Start();

			_listener.BeginGetContext(new AsyncCallback(ListenerCallback),_listener);


		}

		void OnDestroy () {
			if (_listener!=null) {
				//StartCoroutine (KinectServerSender (_stopJson));
				_listener.Close();
			}
		}

		void OnApplicationQuit () {
			if (_listener!=null) {
				StartCoroutine (KinectServerSender (_stopJson));
				_listener.Close();
			}
		}

		/// <summary>
		/// Closes all the streams.
		/// </summary>
		/// <param name="stream">System.IO.Stream stream</param>
		/// <param name="read">System.IO.StreamReader stream</param>
		/// <param name="resp">HttpListenerResponse stream</param>
		private void CloseAll(System.IO.Stream stream, System.IO.StreamReader read, HttpListenerResponse resp) 
		{
			stream.Close();
			read.Close();
			resp.Close();

		}

		/// <summary>
		/// Read the body of the coming post request and update the skeletons attribute.
		/// </summary>
		/// <param name="result">Result </param>
		private void ListenerCallback(IAsyncResult result)
		{
			HttpListener listener = (HttpListener) result.AsyncState;
			// Call EndGetContext to complete the asynchronous operation.
			HttpListenerContext context = listener.EndGetContext(result);
			HttpListenerRequest request = context.Request;
			// Obtain a response object.
			HttpListenerResponse response = context.Response;
			System.IO.Stream body = request.InputStream;
			System.Text.Encoding encoding = request.ContentEncoding;
			System.IO.StreamReader reader = new System.IO.StreamReader (body,encoding);
			string skeletonsSerialized = reader.ReadToEnd ();
			skeletons = JsonHelper.FromJson<Skeleton>(JsonHelper.fixJson(skeletonsSerialized));
			CloseAll (body, reader, response);
			_listener.BeginGetContext(new AsyncCallback(ListenerCallback),_listener);
			return;
		}

		/// <summary>
		/// Send post request to the Kinect server
		/// </summary>
		/// <returns>The server sender.</returns>
		/// <param name="json">Json to send</param>
		private IEnumerator KinectServerSender (string json)
		{
			UnityWebRequest request = new UnityWebRequest (_serverKinect, "POST");
			request.SetRequestHeader ("Content-Type", "application/json");
			byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes (json);
			request.uploadHandler = (UploadHandler)new UploadHandlerRaw (bodyRaw);
			yield return request.SendWebRequest ();

		}


		/// <summary>
		/// Gets the skeletons.
		/// </summary>
		/// <returns>The skeletons.</returns>
		public Skeleton[] GetSkeletons()
		{
			return skeletons;
		}

	}
}
