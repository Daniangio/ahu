﻿using UnityEngine;
using System;

public class KinectData {

	public Vector3[] positions;
	public string[] gestures;

	public int bodyCount()
	{
		return positions.Length;
	}

	public int gesturesCount()
	{
		return gestures.Length;
	}
}
