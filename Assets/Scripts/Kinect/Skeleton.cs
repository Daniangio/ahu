﻿using System;
using UnityEngine;

namespace KinectServerListener
{	
	[Serializable]
	public class Skeleton
	{

		public SerializableVector3 SpineBase;

		public SerializableVector3 SpineMid;

		public SerializableVector3 Neck;

		public SerializableVector3 Head;

		public SerializableVector3 ShoulderLeft;

		public SerializableVector3 ElbowLeft;

		public SerializableVector3 WristLeft;

		public SerializableVector3 HandLeft;

		public SerializableVector3 ShoulderRight;

		public SerializableVector3 ElbowRight;

		public SerializableVector3 WristRight;

		public SerializableVector3 HandRight;

		public SerializableVector3 HipLeft;

		public SerializableVector3 KneeLeft;

		public SerializableVector3 AnkleLeft;

		public SerializableVector3 FootLeft;

		public SerializableVector3 HipRight;

		public SerializableVector3 KneeRight;

		public SerializableVector3 AnkleRight;

		public SerializableVector3 FootRight;

		public SerializableVector3 SpineShoulder;

		public SerializableVector3 HandTipLeft;

		public SerializableVector3 ThumbLeft;

		public SerializableVector3 HandTipRight;

		public SerializableVector3 ThumbRight;

		public string GestureMask;

		public Skeleton ()
		{
		}

//		public bool isBended()
//		{
//			return ( (GestureMask[0] == '1') ? true : false);
//		}

		public bool isTurned()
		{
//			if (GestureMask [0] == '1') { 
//				Debug.Log ("gesturemask è 1 in sk UNITY"); //QUI NON ENTRA MAI
//			}

			return ( (GestureMask[0] == '1') ? true : false);

		}
			
	}
}

