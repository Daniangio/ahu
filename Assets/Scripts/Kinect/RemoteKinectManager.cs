﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinectServerListener;

public class RemoteKinectManager : MonoBehaviour {

	//Game players
	private GameObject[] players;

	private float multiplier_X = 1.1f;
	private float multiplier_Z = -0.67f;
	private float x_offset = 9.8f;
	private float z_offset = 1.3f;

	private int bodyCount;
	private bool readyToTrack = false;

	public KinectListener_Remote kinectListener;
	private KinectData kinectData;
	private int numberOfPlayers;
	private int count;

	public void Initialize () {
		numberOfPlayers = ChoiceStatus.numberOfPlayers;
		kinectData = new KinectData ();
		kinectData.positions = new Vector3[numberOfPlayers];
		kinectData.gestures = new string[numberOfPlayers];

		for (int i = 0; i < numberOfPlayers; i += 1)
			kinectData.positions [i] = Vector3.zero;
		

		for (int i = 0; i < numberOfPlayers; i += 1)
			kinectData.gestures [i] = null;
		readyToTrack = true;
	}

	void Update()
	{
		if (readyToTrack) {
			UpdateKinectData ();
			UpdatePlayers ();
		}
	}

	public void SetupPlayers(int numberOfPlayers, int teamNumber)
	{
		players = new GameObject[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i += 1) {
			string playerName = "Player" + (i + (teamNumber - 1) * numberOfPlayers).ToString ();
			players [i] = GameObject.Find (playerName);
		}

	}

	private void UpdateKinectData()
	{
		Skeleton[] skeletons = kinectListener.GetSkeletons ();
		if (skeletons != null) {
			for (int i = 0; i < numberOfPlayers; i += 1)
				kinectData.positions [i] = Vector3.zero;
			count = 0;
			for (int i = 0; i < skeletons.Length; i += 1) {
				if (skeletons [i].SpineMid.X != 0) {
					kinectData.positions [count] = skeletons [i].SpineMid;

					if (skeletons [i].isTurned ()) { //quello scheletro ha fatto la gesture turn
						Debug.Log ("sk.isTurned() == true RemoteKinectManager"); 
						kinectData.gestures [count] = "Turned"; 
					} else {
						Debug.Log ("sk.isTurned() = false RemoteKinectManager");
						kinectData.gestures [count] = null; 
					}
					if (count < numberOfPlayers -1)
						count += 1;
				}
			}
		}
	}

	private void UpdatePlayers()
	{
		if (Time.deltaTime != 0 && kinectData.positions[0] != Vector3.zero) { //time.deltatime in local non c'è
			for (int i = 0; i < numberOfPlayers; i += 1) {
				float new_pos_x = kinectData.positions [i].x * multiplier_X + x_offset;
				float new_pos_z = kinectData.positions [i].z * multiplier_Z + z_offset;
				players [i].transform.position = new Vector2 (new_pos_x, new_pos_z);

				CheckGestures (i);
			}
		}
	}

	private void CheckGestures (int i)
	{
		//players[player].    oppure players_field_1 in scenemaster?
		//players[player].transform.
//		Skeleton[] skeletons = kinectListener.GetSkeletons ();

		//ora prendi skeletro giusto però kinect data
		// e poi skeletons[i].isTurned() == true -> gesture è stata fatta
//		if (skeletons != null) {
//			if (skeletons [i].isTurned ()) {
//				Debug.Log ("sk.isTurned() == true RemoteKinectManager"); //non ci entra mai
//
//				players [i].GetComponent<PlayerScr> ().gestureTurned = true;
//			} else {
//				players [i].GetComponent<PlayerScr> ().gestureTurned = false;
//				Debug.Log ("sk.isTurned() = false RemoteKinectManager");
//			}
//		}

		if (kinectData.gestures [i] == "Turned") {
			//Debug.Log ("TURNED scritta in kinect data");
			players [i].GetComponent<PlayerScr> ().gestureTurned = true;
		}
		if (kinectData.gestures [i] == null) {
			players [i].GetComponent<PlayerScr> ().gestureTurned = false;
		}

	}

}
