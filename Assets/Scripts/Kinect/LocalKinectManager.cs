﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinectServerListener;

public class LocalKinectManager : MonoBehaviour {

	//Game players
	private GameObject[] players;

	private float multiplier_X = 1.1f;
	private float multiplier_Z = -0.67f;
	private float x_offset = -9.8f;
	private float z_offset = 1.3f;

	private int bodyCount;
	private bool readyToTrack = false;

	public KinectListener_Local kinectListener;
	private KinectData kinectData;
	private int numberOfPlayers;
	private int count;

	private int num;

	public void Initialize () {
		numberOfPlayers = ChoiceStatus.numberOfPlayers;
		kinectData = new KinectData ();
		kinectData.positions = new Vector3[numberOfPlayers];
		//aggiunta Anna
		kinectData.gestures = new string[numberOfPlayers];

		for (int i = 0; i < numberOfPlayers; i += 1)
			kinectData.positions [i] = Vector3.zero;
		//readyToTrack = true;

		//aggiunta Anna, se non usi kinectDatA NON SERVE
		for (int i = 0; i < numberOfPlayers; i += 1)
			kinectData.gestures [i] = null;
		readyToTrack = true;

	}

	void Update()
	{
		if (readyToTrack) {
			UpdateKinectData ();
			UpdatePlayers ();
		}
	}

	public void SetupPlayers(int numberOfPlayers, int teamNumber)
	{
		players = new GameObject[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i += 1) {
			string playerName = "Player" + (i + (teamNumber - 1) * numberOfPlayers).ToString ();
			players [i] = GameObject.Find (playerName);
		}

	}

	private void UpdateKinectData()
	{
		Skeleton[] skeletons = kinectListener.GetSkeletons ();
		if (skeletons != null) {
			for (int i = 0; i < numberOfPlayers; i += 1)
				kinectData.positions [i] = Vector3.zero;
			count = 0;
			for (int i = 0; i < skeletons.Length; i += 1) {
				if (skeletons [i].SpineMid.X != 0) {
					kinectData.positions [count % numberOfPlayers] = skeletons [i].SpineMid;

					//aggiunto da anna
					if (skeletons [i].isTurned ()) { //quello scheletro ha fatto la gesture turn
						Debug.Log ("sk.isTurned() == true LocalKinectManager"); 
						kinectData.gestures [count % numberOfPlayers] = "Turned"; 
					} else {
						Debug.Log ("sk.isTurned() = false LocalKinectManager");
						kinectData.gestures [count % numberOfPlayers] = null; 
					}

					count += 1;
				}
//				//se fai la gesture alza la mano
//				if (skeletons[i].HandLeft.Y>skeletons[i].Head.Y || skeletons[i].HandRight.Y > skeletons[i].Head.Y){
//					kinectData.gestures [count % numberOfPlayers] = "HandUp";
//				}

			}

			num = count; //quanti scheletri diversi da zero
		}
	}


	public int GetNumberOfTrackedBodies()
	{
		return num;
	}


	private void UpdatePlayers()
	{
		if (kinectData.positions[0] != Vector3.zero) {
			for (int i = 0; i < numberOfPlayers; i += 1) {

				float new_pos_x = kinectData.positions [i].x * multiplier_X + x_offset;
				float new_pos_z = kinectData.positions [i].z * multiplier_Z + z_offset;
				players [i].transform.position = new Vector2 (new_pos_x, new_pos_z);

				CheckGestures (i);
			}
		}
	}

	//chiamata per ogni player
	private void CheckGestures (int i)
	{
		//players[player].    oppure players_field_1 in scenemaster?
		//players[player].transform.

		//Skeleton[] skeletons = kinectListener.GetSkeletons ();

		//ora prendi skeletro giusto però kinect data
		// e poi skeletons[i].isTurned() == true -> gesture è stata fatta
//		if (skeletons != null) {
//			Debug.Log (skeletons[i].GestureMask);
//			if (skeletons [i].isTurned ()) {
//				Debug.Log ("sk.isTurned() == true LocalKinectManager");
//				players [i].GetComponent<PlayerScr> ().gestureTurned = true;
//			} else {
//				players [i].GetComponent<PlayerScr> ().gestureTurned = false;
//				Debug.Log ("sk.isTurned() = false LocalKinectManager");
//			}
//		}
//	
//		if (kinectData.gestures[i] == "HandUp" && i < num){
//				players [i].GetComponent<PlayerScr> ().gestureTurned = true;
//			}
				
		if (kinectData.gestures [i] == "Turned") {
			//Debug.Log ("TURNED scritta in kinect data");
			players [i].GetComponent<PlayerScr> ().gestureTurned = true;
		}
		if (kinectData.gestures [i] == null) {
			players [i].GetComponent<PlayerScr> ().gestureTurned = false;
		}

	}

}
