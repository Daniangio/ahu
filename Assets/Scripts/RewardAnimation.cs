﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RewardAnimation : MonoBehaviour {

	// camera 1 è quella della squadra 1
	public int team; //1 ha vinto la prima squadra
	public GameObject goldMedal;
	public GameObject silverMedal;
	int winner;


	// Use this for initialization
	void Start () {
		winner = ChoiceStatus.winner;
		LedBarMaster.ShowEffect (3, 2, 10);
		StartCoroutine (AnimationBiggerObj ());
	}

	IEnumerator AnimationBiggerObj(){
		float scaleMedal = 1;
		if (winner == 0) {		
			goldMedal.SetActive (true);

			for(int i = 1; i<= 50;i++){
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 50; i > 25; i--) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 25; i <= 50; i++) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			yield return new WaitForSeconds (5f);

			SceneManager.LoadScene("MenuIniziale");

		} else
		if (winner == 1 && team == 1) {		
			goldMedal.SetActive (true);

			for(int i = 1; i<= 50;i++){
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 50; i > 25; i--) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}
		
			for (int i = 25; i <= 50; i++) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			yield return new WaitForSeconds (5f);

			SceneManager.LoadScene("MenuIniziale");

		} else
		if (winner == 1 && team == 2) {  
			silverMedal.SetActive(true);
			for(int i = 1; i<= 50;i++){
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 50; i > 25; i--) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 25; i <= 50; i++) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			yield return new WaitForSeconds (5f);

			SceneManager.LoadScene("MenuIniziale");
		} else
		if (winner == 2 && team == 2) { 
			
			goldMedal.SetActive (true);

			for(int i = 1; i<= 50;i++){
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 50; i > 25; i--) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 25; i <= 50; i++) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				goldMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			yield return new WaitForSeconds (5f);

			SceneManager.LoadScene("MenuIniziale");
		} else
		if (winner == 2 && team == 1) { 
			silverMedal.SetActive(true);
			for(int i = 1; i<= 50;i++){
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 50; i > 25; i--) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			for (int i = 25; i <= 50; i++) {
				scaleMedal = Mathf.Log ((float)i);
				scaleMedal = scaleMedal * 10;
				silverMedal.transform.localScale = new Vector3 (scaleMedal, scaleMedal, 1);
				yield return new WaitForSeconds (0.03f);
			}

			yield return new WaitForSeconds (5f);

			SceneManager.LoadScene("MenuIniziale");

		}


	}

}
