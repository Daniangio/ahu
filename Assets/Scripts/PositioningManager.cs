﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositioningManager : MonoBehaviour {

	int progressionStory;
	public VoiceManagerPositioning audioManager;
	public SpriteRenderer back_1;
	public SpriteRenderer back_2;

	public GameObject framePrefab;
	public Sprite[] globalBackImages; //Tutte le immagini di background

	private int firstIndex; //Indice background primo campo
	private int secondIndex; //Indice background secondo campo
	private int audioIndex; //Indice audio iniziale
	private int finalIndex; //indice audio finale

	//servono se devo mettere cerchi in cui giocatori si devono posizionare
	private int numberPlayers;
	private int numberPlayersPresent = 0;

	public bool commandDone = false;


	// Use this for initialization
	void Start () {
		progressionStory = ChoiceStatus.getProgressionStory ();
		SelectDataForThisScene ();
		Dispose ();
		audioManager.StartAudio (audioIndex);
		//LedBarMaster.Initialize (10);
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Return)) {
			audioManager.FinalAudio (0);
			//ChoiceStatus.LoadNextGameScene ();
		}
	}

	/// <summary>
	/// Initialize the data for this scene.
	/// </summary>
	private void SelectDataForThisScene ()
	{
		ChoiceStatus.mode = "Collaborative"; //da togliere
		if (ChoiceStatus.mode == "Collaborative") {
			switch (progressionStory) {
			case 0:
				firstIndex = 0;
				secondIndex = 0;
				audioIndex = 1; 
				numberPlayers = 2;
				break;
			case 1:
				firstIndex = 1;
				secondIndex = 1;
				audioIndex = 2;
				numberPlayers = 2;
				break;
			case 2:
				firstIndex = 0;
				secondIndex = 1;
				audioIndex = 3;
				numberPlayers = 4;
				break;
			}
		}

		if (ChoiceStatus.mode == "Competitive") {
			//switch (progressionStory) {
			//case 0:
				firstIndex = 0;
				secondIndex = 1;
				audioIndex = 3;  //da settare bene
				numberPlayers = 2;
				//break;
//			case 1:
//				firstIndex = 0;
//				secondIndex = 1;
//				audioIndex = 3;
//				numberPlayers = 2;
//				break;

		}

	}


	private void Dispose ()
	{
		var width1 = globalBackImages [firstIndex].bounds.size.x;
		var height1 = globalBackImages [firstIndex].bounds.size.y;
		var width2 = globalBackImages [secondIndex].bounds.size.x;
		var height2 = globalBackImages [secondIndex].bounds.size.y;
		var worldScreenHeight = Camera.allCameras [0].orthographicSize * 2.0;
		var worldsScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		back_1.sprite = globalBackImages[firstIndex];
		back_2.sprite = globalBackImages [secondIndex];
		back_1.transform.localScale = new Vector3 ((float)worldsScreenWidth / width1, (float)worldScreenHeight / height1);
		back_2.transform.localScale = new Vector3 ((float)worldsScreenWidth / width2, (float)worldScreenHeight / height2);
	}



	public void NextScene ()
	{
		StartCoroutine (waiter());
	}

	IEnumerator waiter()
	{
		yield return new WaitForSeconds (1.5f);
		ChoiceStatus.LoadNextGameScene ();
	}

}
