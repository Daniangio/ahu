﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData {

	public FrameData[] frames;
	public string message;
	public string audioName;
	public string color;

}