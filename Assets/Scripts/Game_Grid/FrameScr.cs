﻿using System;
using System.Collections;
using UnityEngine;

public class FrameScr : MonoBehaviour {

	private SceneMasterScr sceneMaster;
	private FramesOrganizer framesOrganizer;
    
    private bool isSelected = false;
	public int teamNumber = 0;
	public bool isCorrect;
	public SpriteRenderer element;
	public SpriteRenderer context;

	private int i; //Used to move the frame into the correct sector of the screen
	private int j;


	private float maxDistance_x;
	private float maxDistance_y;

	void Start() {
		
	}


	/// <summary>
	/// Initialize the specified framesOrganizerName, spriteName, isCorrect, i and j case nina e dori.
	/// </summary>
	/// <param name="framesOrganizerName">Frames organizer name.</param>
	/// <param name="spriteName">Sprite name.</param>
	/// <param name="isCorrect">If set to <c>true</c> is correct.</param>
	/// <param name="i">The index.</param>
	/// <param name="j">J.</param>
	public void Initialize(string framesOrganizerName, string spriteName, bool isCorrect, int i, int j)
	{
		sceneMaster = GameObject.Find ("SceneMaster").GetComponent<SceneMasterScr> ();
		framesOrganizer = GameObject.Find (framesOrganizerName).GetComponent<FramesOrganizer> ();
		//Load element sprite
		int index = (int)Char.GetNumericValue(spriteName[spriteName.Length-1]) + 1;
		spriteName = spriteName.Substring(0,spriteName.Length - 2);
		string filePath = "Sprites/" + spriteName;
		var sprites  = Resources.LoadAll(filePath);
		element.sprite = (Sprite)sprites[index];
		if (element.sprite == null)
			Debug.Log ("SPRITE NON CARICATA");

		//Move the element in the center of the context
		element.transform.position = new Vector3 (transform.position.x, transform.position.y);

		//Load and adapt to screen the context sprite
		filePath = "Sprites/" + "rect";
		var spr = Resources.LoadAll(filePath);
		Sprite contextSpr = (Sprite)spr[1];
		context.sprite = contextSpr;
		context.color = new Color (0, 0, 0, 0.2f);

		this.isCorrect = isCorrect;
		if (isCorrect == true)
			sceneMaster.AddTotalCorrect(teamNumber);
		this.isSelected = false;

		this.i = i;
		this.j = j;

		ResizeSpriteToScreen (context, 0.95f, false);
		ResizeSpriteToScreen (element, 0.85f, true);
	}
		



	/// <summary>
	/// Every time it finds the nearest player, select and deselect the frame.
	/// </summary>
	void Update()
	{
		if (teamNumber > 0) {
			GameObject nearestPlayer = FindClosest (transform.position, "Player");
			if (IsOverMe (nearestPlayer.transform.position) && !isSelected) {
				Select ();
			}
			if (!IsOverMe (nearestPlayer.transform.position) && isSelected) {
				Select ();
			}
		}
	}

	private void ResizeSpriteToScreen(SpriteRenderer sr, float size, bool keepRatio) {
		if (sr == null)
			return;

		var worldScreenHeight = Camera.allCameras[teamNumber - 1].orthographicSize * 2;
		var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		Vector2 local_sprite_size = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

		float frame_width = (worldScreenWidth - framesOrganizer.border_left - framesOrganizer.border_right) / framesOrganizer.horizontalFrames;
		float frame_height = (worldScreenHeight - framesOrganizer.border_top) / framesOrganizer.verticalFrames;
		float myScale_x;
		float myScale_y;
		if (keepRatio) {
			if (local_sprite_size.x < local_sprite_size.y) {
				myScale_y = (1 / local_sprite_size.y) * frame_height * size;
				myScale_x = myScale_y;
				if (local_sprite_size.x * myScale_x > frame_width) {
					myScale_x = (1 / local_sprite_size.x) * frame_width * size;
					myScale_y = myScale_x;
					if (local_sprite_size.y * myScale_y > frame_height) {
						myScale_x = (1 / local_sprite_size.x) * frame_width * size;
						myScale_y = (1 / local_sprite_size.y) * frame_height * size;
					}
				}
			} else {
				myScale_x = (1 / local_sprite_size.x) * frame_width * size;
				myScale_y = myScale_x;
			}
		}
		else {
			myScale_x = (1 / local_sprite_size.x) * frame_width * size;
			myScale_y = (1 / local_sprite_size.y) * frame_height * size;
		}
		sr.transform.localScale = new Vector3 (myScale_x, myScale_y, 1);

			float offset_x = frame_width + Camera.allCameras [teamNumber - 1].transform.position.x + framesOrganizer.border_right - framesOrganizer.border_left;
			float offset_y = frame_height + Camera.allCameras [teamNumber - 1].transform.position.y - framesOrganizer.border_top / 2;

			transform.position = new Vector3 (i * frame_width + offset_x, j * frame_height + offset_y, 0);
			maxDistance_x = local_sprite_size.x * myScale_x / 2;
			maxDistance_y = local_sprite_size.y * myScale_y / 2;



	}


	private bool IsOverMe(Vector3 pos)
	{
		if (pos.x > transform.position.x - maxDistance_x && pos.x < transform.position.x + maxDistance_x && pos.y > transform.position.y - maxDistance_y && pos.y < transform.position.y + maxDistance_y)
			return true;
		return false;
	}

	public void Select()
	{
		isSelected = !isSelected;
		string spriteName;
		int offsetSprite;
		Color col;
		if (isSelected == false) {
			spriteName = "rect";
			col = new Color (0, 0, 0, 0.2f);
			offsetSprite = 1;
		} else {
			spriteName = "rect";
			col = new Color (255, 255, 255, 0.2f);
			offsetSprite = 1;
		}
		if (isSelected == true && isCorrect == true)
			sceneMaster.AddCorrect (teamNumber, 1);
		else if (isSelected == false && isCorrect == true)
			sceneMaster.RemoveCorrect (teamNumber, 1);
		else if (isSelected == true && isCorrect == false)
			sceneMaster.RemoveCorrect (teamNumber, 1);
		else if (isSelected == false && isCorrect == false)
			sceneMaster.AddCorrect (teamNumber, 1);

		string filePath = "Sprites/" + spriteName;
		var spr  = Resources.LoadAll(filePath);
		context.sprite = (Sprite)spr[offsetSprite];
		context.color = col;
	}


	/*IEnumerator playSelectSound(){
	 * selecSound.Play();
	 * }
	 * 
	 * IEnumerator playnotSelectSound(){
	 * notSelecSound.Play();
	 * }
		
	} */

	public GameObject FindClosest(Vector3 position, string tag)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(tag);
		GameObject closest = null;
		float distance = Mathf.Infinity;
		foreach (GameObject go in gos)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

    }