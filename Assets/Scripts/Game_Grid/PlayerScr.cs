﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScr : MonoBehaviour {

	public FrameScrMemo actualFrame;
	public bool gestureTurned = false;

	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer> ().transform.localScale = new Vector3 (0.1f, 0.1f, 0);
		actualFrame = null;
	}
	
	// Update is called once per frame
	void Update () {
		/*var pos = Input.mousePosition;
		pos = Camera.main.ScreenToWorldPoint(pos);
		transform.position = pos;

		if (Input.GetMouseButtonDown (0))
			PerformAction ();*/
	}

	public void SetPosition(Vector3 pos)
	{
		transform.position = pos;
	}

	public void PerformAction()
	{
		GameObject closest = FindClosest (transform.position, "Frame");
		FrameScr frame = closest.GetComponent<FrameScr> ();
		frame.Select ();
	}

	public GameObject FindClosest(Vector3 position, string tag)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(tag);
		GameObject closest = null;
		float distance = Mathf.Infinity;
		foreach (GameObject go in gos)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

	public void setFrame(FrameScrMemo frame){
		this.actualFrame = frame;
	}

	//metodo riceve gesture e chiama control gesture in scene master il quale riceve che frame del player ha fatto gesture e vede se la può fare. forse vede anche player.


}
