﻿using System;
using System.Collections;
using UnityEngine;

[System.Serializable]
public class FrameBuilderScr : MonoBehaviour {

	private string spriteName;
	private int offsetSprite;
	private bool isCorrect;

	public FrameBuilderScr(string spriteName, int offsetSprite, bool isCorrect)
	{
		this.spriteName = spriteName;
		this.offsetSprite = offsetSprite;
		this.isCorrect = isCorrect;
	}

	public string getSpriteName()
	{
		return this.spriteName;
	}

	public int getOffsetSprite()
	{
		return this.offsetSprite;
	}

	public bool getIsCorrect()
	{
		return this.isCorrect;
	}

}