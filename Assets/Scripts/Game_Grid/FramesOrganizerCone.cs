﻿using System;
using System.Collections;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;


public class FramesOrganizerCone : MonoBehaviour {

	private SceneMasterScrMemo sceneMaster; 
	public GameObject framePrefab;
	private GameObject[] framesDisposed; //left
	private GameObject[] framesDisposedRight;
	private FrameData[] frames;
	//private FrameData[] framesRight;
	public int fieldNumber;

	public Text textMessage;
	private string message;

	public int effect = 1;

	private Iterator<string> dataPool;
	private string gameDataFileName;

	private float width;
	private float height;
	public float border_left;
	public float border_right;
	public float border_top;
	public float center;
	private float playground_w;
	private float playground_h;
	public int horizontalFrames; //max
	public int verticalFrames;  //max
	public int numElements = 4;

	private int numRounds;

	public AudioClip stageCorrectSound;
	public AudioClip stageCorrectSoundSecond;

	void Start () {
		sceneMaster = GameObject.Find ("SceneMaster").GetComponent<SceneMasterScrMemo> (); 
		ChoiceStatus.story = "Memo"; // da eliminare
		dataPool = new Iterator<string> ();
		string[] dataPoolFiles = SessionManager.GetJsonFiles(fieldNumber);
		FillDataPool (dataPoolFiles);
		LoadGameData ();
	}

	public void InitializeSession() {
		textMessage.text = message;
		AdaptFieldDimensions ();
		Dispose();
	}


	private void Dispose()
	{
//		if (ChoiceStatus.level == 1){
//		numElements = 4; //in base al livello, settato da qualche parte o anche qui oppure = frames.Lenght
//		} else {
//			numElements = 6;
//		}

		numElements = frames.Length;

		//Dispose into the room
		framesDisposed = new GameObject[numElements]; 
		framesDisposedRight = new GameObject[numElements];

		 // left sector
		string sector = "left";
		bool isRounded = false;

		//RandomizeArray (frames);   DA RIAGGIUNGERE ASSOLUTAMENTE, NO RANDOM COSì PIù FACILE

		for (int i = 0; i < numElements; i += 1)
		{
			//FrameScrMemo framepre; se non riesci a fare prefab allora fai in altro modo
			GameObject frameGO = GameObject.Instantiate (framePrefab, new Vector3 (0, 0, 0), Quaternion.identity); //framePrefab non deve avere anche altro? tipo id ecc
			FrameScrMemo frameScr = frameGO.GetComponent<FrameScrMemo>();
			frameScr.fieldNumber = fieldNumber;

			frameScr.Initialize(this.name, frames[i].spriteName, frames[i].id, i, sector, isRounded);

			framesDisposed [i] = frameGO;
		}

		//right sector
		sector = "right";

		//RandomizeArray (frames);  DA RIAGGIUNGERE ASSOLUTAMENTE, NO RANDOM COSì PIù FACILE

		for (int i = 0; i < numElements; i += 1)
		{
			GameObject frameGO = GameObject.Instantiate (framePrefab, new Vector3 (0, 0, 0), Quaternion.identity);
			FrameScrMemo frameScr = frameGO.GetComponent<FrameScrMemo>();
			frameScr.fieldNumber = fieldNumber;
	
			frameScr.Initialize(this.name, frames[i].spriteName, frames[i].id, i, sector, isRounded);

			framesDisposedRight [i] = frameGO;
		}

		//sceneMaster.StartGame (fieldNumber);

	}

//	private FrameData[] Reorder (FrameData[] frames, int index, bool findCorrect)
//	{
//		for (int i = index + 1; i < frames.Length; i += 1) {
//			if (frames [i].isCorrect == findCorrect) {
//				FrameData temp = frames[index];
//				frames [index] = frames [i];
//				frames [i] = temp;
//				return frames;
//			}
//		}
//		return frames;
//	}

	/// <summary>
	/// Load into "elements" the data read on a json file, used to setup the game
	/// </summary>
	private void LoadGameData()
	{
		gameDataFileName = dataPool.Next ();

		string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
			message = loadedData.message;
			string audioPath = Path.Combine ("AudioRecords", loadedData.audioName);
			if (fieldNumber == 1) {
				AudioClip clip = Resources.Load<AudioClip> (audioPath);
				if (clip != null) {
					sceneMaster.PlayInitialAudio (clip);
				}
			}
		}
		else
		{
			Debug.LogError("Cannot load game data!");
		}
	}


	/// <summary>
	/// Randomizes the array of the frameData.
	/// </summary>
	/// <param name="frames">Frames taken by the json files</param>
	private void RandomizeArray(FrameData[] frames)
	{
		for (var i = frames.Length - 1; i > 0; i -= 1) {
			var r = UnityEngine.Random.Range (0, i);
			var temp = frames [i];
			frames [i] = frames [r];
			frames [r] = temp;
		}
	}

	public void NewSession() {
		//Delete all frames on ground
		foreach (GameObject go in framesDisposed) {
			Destroy (go);
		}
		StartCoroutine (waiter ());
	}



	IEnumerator waiter()
	{
		if (!dataPool.IsEmpty ()) {
			yield return new WaitForSeconds (1);
			textMessage.text = "Preparati per il prossimo round";
			yield return new WaitForSeconds (1);
			InitializeSession ();
		}
		else {
//			if (sceneMaster.teamsWhoEnded == 0) {
//				textMessage.text = "Hai finito per primo! Hai guadagnato 2 punti.\nAspetta i tuoi avversari";  //
//				sceneMaster.PlaySound (stageCorrectSound);
//			} else if (sceneMaster.teamsWhoEnded == 1) {
//				textMessage.text = "Hai finito per secondo. Hai guadagnato 1 punto!";
//				sceneMaster.PlaySound (stageCorrectSoundSecond);
//				yield return new WaitForSeconds (2);
//			}
//			sceneMaster.teamsWhoEnded += 1;
		}
	}


	public void FillDataPool(string[] dataFileNames)
	{
		dataPool.AddToPool (dataFileNames);
	}

	/// <summary>
	/// Based on the number of elements in the Json file set verticalFrames and horizontalFrames.
	/// </summary>
	private void AdaptFieldDimensions()
	{
		//ChoiceStatus.level = 1;

		if (ChoiceStatus.level == 1) {
			verticalFrames = 3;
			horizontalFrames = 4;
		} else {
			verticalFrames = 3;
			horizontalFrames = 6;
		}
	}


	/// <summary>
	/// Tells if there is a rouded card in the left sector of the field
	/// </summary>
	/// <returns><c>true</c>, if at least one card in the left side of the field was roundeded, <c>false</c> otherwise.</returns>
	public bool isRoundedCardLeft(){
		bool finded = false;
		FrameScrMemo frameScr;
		foreach (GameObject frame in framesDisposed) {
		//for (int i = 0; i < framesDisposed.Length; i += 1) {
			//frameScr = framesDisposed [i].GetComponent<FrameScrMemo> ();
			if (frame != null) {
				frameScr = frame.GetComponent<FrameScrMemo> ();
				if (frameScr.isRounded == true) {
					finded = true;
					//posso anche uscire prima dal for
				}
			}
		}
		return finded;
	}


	/// <summary>
	/// Tells if there is a rouded card in the right sector of the field
	/// </summary>
	/// <returns><c>true</c>, if at least one card in the right side of the field was roundeded, <c>false</c> otherwise.</returns>
	public bool isRoundedCardRight(){
		bool finded = false;
		FrameScrMemo frameScr;
		foreach (GameObject frame in framesDisposedRight) {
		//for (int i = 0; i < framesDisposedRight.Length; i += 1) {
			//frameScr = framesDisposedRight [i].GetComponent<FrameScrMemo> ();
			if (frame != null) {
				frameScr = frame.GetComponent<FrameScrMemo> ();
				if (frameScr.isRounded == true) {
					finded = true;
					//posso anche uscire prima dal for
				}
			}
		}
		return finded;
	}

	 
	public void destroyCardsRounded(){         //se faccio destroy elimina anche dall'array?
		for (int i = 0; i< framesDisposed.Length; i +=1) {
			if (framesDisposed[i].GetComponent<FrameScrMemo> ().isRounded == true) {
				Destroy (framesDisposed[i]); //vedi se destroy non elimina già dall'array ma non credo
				framesDisposed = framesDisposed.Where (w => w != framesDisposed[i]).ToArray();
			}
		}
		for (int i = 0; i< framesDisposedRight.Length; i +=1) {
			if (framesDisposedRight[i].GetComponent<FrameScrMemo> ().isRounded == true) {
				Destroy (framesDisposedRight[i]);
				framesDisposedRight = framesDisposedRight.Where (w => w != framesDisposed[i]).ToArray();
			}
		}
	}


	public bool isFieldEmpty(){
		return (framesDisposed.Length == 0) && (framesDisposedRight.Length == 0);
	}



	public GameObject[] roundedCards(){
		GameObject[] rounded = new GameObject[2];
		foreach (GameObject go in framesDisposed) {
			if (go.GetComponent<FrameScrMemo> ().isRounded == true) {
				rounded [0] = go;
			}
		}
		foreach (GameObject go in framesDisposedRight) {
			if (go.GetComponent<FrameScrMemo> ().isRounded == true) {
				rounded [1] = go;
			}
		}
		return rounded;
	}




}

