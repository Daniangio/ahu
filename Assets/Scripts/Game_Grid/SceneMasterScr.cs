﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using HueLights;

public class SceneMasterScr : MonoBehaviour {

	private int totalCorrectElements_1;
	private int actualCorrectElements_1;
	private int totalCorrectElements_2;
	private int actualCorrectElements_2;
	private bool gameStarted_1;
	private bool gameStarted_2;
	public int teamsWhoEnded;

	private int numberOfPlayers;
	public GameObject playerPrefab;
	private GameObject[] players_team_1;
	private GameObject[] players_team_2;
	public LocalKinectManager localKinectManager;
	public RemoteKinectManager remoteKinectManager;


	public FramesOrganizer framesOrganizer_1;
	public FramesOrganizer framesOrganizer_2;
	public Text textMessage_1;
	public Text textMessage_2;

	private AudioSource audioSource;
	public AudioClip initialAudio;
	public AudioClip finalAudio;

	// Use this for initialization
	void Startup () { 
		audioSource = GetComponent<AudioSource> ();

		numberOfPlayers = ChoiceStatus.numberOfPlayers;

		HueLightControl c = GameObject.FindGameObjectWithTag ("HueLight").GetComponent<HueLightControl>();
		c.stop();

		//Setup first team
		players_team_1 = new GameObject[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i += 1) {
			GameObject player = Instantiate (playerPrefab, new Vector3 (0, 0), Quaternion.identity);
			player.name = "Player" + i.ToString ();
			players_team_1 [i] = player;
		}

		//Setup second team
		players_team_2 = new GameObject[numberOfPlayers];
		for (int i = numberOfPlayers; i < numberOfPlayers * 2; i += 1) {
			GameObject player = Instantiate (playerPrefab, new Vector3 (0, 0), Quaternion.identity);
			player.name = "Player" + i.ToString ();
			players_team_2 [i-numberOfPlayers] = player;
		}

		teamsWhoEnded = 0;

		localKinectManager.SetupPlayers (numberOfPlayers, 1);
		localKinectManager.Initialize ();
		remoteKinectManager.SetupPlayers (numberOfPlayers, 2);
		remoteKinectManager.Initialize ();

		//Initialize scores and first session
		totalCorrectElements_1 = 0;
		actualCorrectElements_1 = 0;
		totalCorrectElements_2 = 0;
		actualCorrectElements_2 = 0;
		gameStarted_1 = false;
		gameStarted_2 = false;
	}

	public void PlayInitialAudio(AudioClip clip)
	{
		Startup ();
		initialAudio = clip;
		StartCoroutine (InitialAudio ());
	}

	IEnumerator InitialAudio()
	{
		audioSource.clip = initialAudio;
		audioSource.Play();
		yield return new WaitWhile (() => audioSource.isPlaying);
		framesOrganizer_1.InitializeSession ();
		framesOrganizer_2.InitializeSession ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow) && Time.timeScale == 1) {
			AddCorrect(1, 10);
			//LedBarMaster.ShowEffect (2, 1, 5000);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow) && Time.timeScale == 1) {
			AddCorrect(2, 10);
			//LedBarMaster.ShowEffect (2, 10, 5000);
		}


		if (gameStarted_1 == true && actualCorrectElements_1 >= numberOfPlayers) {   //e se in tutti i frame selezionati c'è un corpo sopra
			textMessage_1.text = "Complimenti! Hai fatto un punto!";
			LedBarMaster.AddPoints (1, 1);
			LedBarMaster.ShowEffect (1, 3, 10000);
			ResetStage (1);
			framesOrganizer_1.NewSession ();
		}
		if (gameStarted_2 == true && actualCorrectElements_2 >= numberOfPlayers) { //actualcorrectsElements = numplayer perchè se metto totalcorrects = 6 ma i giocatori sono 4 non vincono mai
			textMessage_2.text = "Complimenti! Hai fatto un punto!";
			LedBarMaster.AddPoints (1, 1);
			LedBarMaster.ShowEffect (2, 3, 10000);
			ResetStage (2);
			framesOrganizer_2.NewSession ();
		}

		if (teamsWhoEnded == 2) {
			StartCoroutine (End ());
			teamsWhoEnded = 0;
		}

	}

	IEnumerator End ()
	{
		//audioSource.clip = finalAudio;
		//audioSource.Play();
		//yield return new WaitWhile (() => audioSource.isPlaying);
		yield return new WaitForSeconds (1);
		ChoiceStatus.LoadNextStoryScene ();
	}

	public void PlaySound(AudioClip clip)
	{
		audioSource.clip = clip;
		audioSource.Play();
	}

	private void ResetStage(int teamNumber)
	{
		if (teamNumber == 1) {
			if (teamsWhoEnded == 0)
				ChoiceStatus.teamPoints_1 += 2;
			else
				ChoiceStatus.teamPoints_1 += 1;
			gameStarted_1 = false;
			totalCorrectElements_1 = 0;
			actualCorrectElements_1 = 0;
		} else if (teamNumber == 2) {
			if (teamsWhoEnded == 0)
				ChoiceStatus.teamPoints_2 += 2;
			else
				ChoiceStatus.teamPoints_2 += 1;
			gameStarted_2 = false;
			totalCorrectElements_2 = 0;
			actualCorrectElements_2 = 0;
		}

	}

	public void StartGame(int teamNumber)
	{
		if (teamNumber == 1)
			gameStarted_1 = true;
		else
			gameStarted_2 = true;
	}

	public void AddTotalCorrect(int teamNumber)
	{
		if (teamNumber == 1)
			totalCorrectElements_1 += 1;
		else
			totalCorrectElements_2 += 1;
	}

	public void AddCorrect (int teamNumber, int points)
	{
		if (teamNumber == 1)
			actualCorrectElements_1 += points;
		else
			actualCorrectElements_2 += points;
	}

	public void RemoveCorrect (int teamNumber, int points)
	{
		if (teamNumber == 1)
			actualCorrectElements_1 -= points;
		else
			actualCorrectElements_2 -= points;
	}

}
