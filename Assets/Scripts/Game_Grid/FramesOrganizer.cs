﻿using System;
using System.Collections;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;


public class FramesOrganizer : MonoBehaviour {

	private SceneMasterScr sceneMaster;
	public GameObject framePrefab;
	private GameObject[] framesDisposed;
	private FrameData[] frames;
	public int teamNumber;

	public Text textMessage;
	private string message;
	
	public int effect = 1;

	private Iterator<string> dataPool;
    private string gameDataFileName;

    private float width;
    private float height;
	public float border_left;
	public float border_right;
	public float border_top;
	private float playground_w;
	private float playground_h;
    public int horizontalFrames;
    public int verticalFrames;


	private int numRounds;

	public AudioClip stageCorrectSound;
	public AudioClip stageCorrectSoundSecond;

    void Start () {
		sceneMaster = GameObject.Find ("SceneMaster").GetComponent<SceneMasterScr> ();

		dataPool = new Iterator<string> ();
		int field = 0;
		string[] dataPoolFiles = SessionManager.GetJsonFiles(field);
		FillDataPool (dataPoolFiles);
		LoadGameData ();
	}

	public void InitializeSession() {
		textMessage.text = message;
		AdaptFieldDimensions ();
		Dispose ();

	}


	/// <summary>
	// Dispose the frames along the screen, adapting them to the screen size and initializing them with values loaded from a json file
	/// </summary>
    private void Dispose()
    {
		//Dispose into the room
		framesDisposed = new GameObject[horizontalFrames * verticalFrames];
        int count = 0;
		//bool gotCorrect;

		for (int i = -(horizontalFrames + 1) / 2; i < horizontalFrames / 2; i += 1)
        {
			//gotCorrect = false;
			for (int j = -(verticalFrames + 1) / 2; j < verticalFrames / 2; j += 1)
            {
				GameObject frameGO = GameObject.Instantiate (framePrefab, new Vector3 (0, 0, 0), Quaternion.identity);
				FrameScr frameScr = frameGO.GetComponent<FrameScr>();
				frameScr.teamNumber = teamNumber;
				/*if (gotCorrect && frames[count].isCorrect)
					frames = Reorder (frames, count, false);
				if (!gotCorrect && j == verticalFrames / 2 - 1 && !frames [count].isCorrect)
					frames = Reorder (frames, count, true);*/
				frameScr.Initialize(this.name, frames[count].spriteName, frames[count].isCorrect, i, j);
				/*if (frames [count].isCorrect)
					gotCorrect = true;*/
				framesDisposed [count] = frameGO;
                count += 1;
            }
        }
		sceneMaster.StartGame (teamNumber);
    }



	private FrameData[] Reorder (FrameData[] frames, int index, bool findCorrect)
	{
		for (int i = index + 1; i < frames.Length; i += 1) {
			if (frames [i].isCorrect == findCorrect) {
				FrameData temp = frames[index];
				frames [index] = frames [i];
				frames [i] = temp;
				return frames;
			}
		}
		return frames;
	}

	/// <summary>
	/// Load into "elements" the data read on a json file, used to setup the game
	/// </summary>
    private void LoadGameData()
    {
		gameDataFileName = dataPool.Next ();

        string filePath = Path.Combine(Application.streamingAssetsPath, "JsonFiles");
		filePath = Path.Combine (filePath, gameDataFileName);
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
			GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
			frames = loadedData.frames;
			message = loadedData.message;
			string audioPath = Path.Combine ("AudioRecords", loadedData.audioName);
			if (teamNumber == 1) {
				AudioClip clip = Resources.Load<AudioClip> (audioPath);
				if (clip != null) {
					sceneMaster.PlayInitialAudio (clip);
				}
			}
			//RandomizeArray (frames);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }
    }


	/// <summary>
	/// Randomizes the array of the frameData.
	/// </summary>
	/// <param name="frames">Frames taken by the json files</param>
	private void RandomizeArray(FrameData[] frames)
	{
		for (var i = frames.Length - 1; i > 0; i -= 1) {
			var r = UnityEngine.Random.Range (0, i);
			var temp = frames [i];
			frames [i] = frames [r];
			frames [r] = temp;
		}
	}

	public void NewSession() {
		//Delete all frames on ground
		foreach (GameObject go in framesDisposed) {
			Destroy (go);
		}
		StartCoroutine (waiter ());
	}

	public void FillDataPool(string[] dataFileNames)
	{
		dataPool.AddToPool (dataFileNames);
	}

	IEnumerator waiter()
	{
		if (!dataPool.IsEmpty ()) {
			yield return new WaitForSeconds (1);
			textMessage.text = "Preparati per il prossimo round";
			yield return new WaitForSeconds (1);
			InitializeSession ();
		}
		else {
			if (sceneMaster.teamsWhoEnded == 0) {
				textMessage.text = "Hai finito per primo! Hai guadagnato 2 punti.\nAspetta i tuoi avversari";
				sceneMaster.PlaySound (stageCorrectSound);
			} else if (sceneMaster.teamsWhoEnded == 1) {
				textMessage.text = "Hai finito per secondo. Hai guadagnato 1 punto!";
				sceneMaster.PlaySound (stageCorrectSoundSecond);
				yield return new WaitForSeconds (2);
			}
			sceneMaster.teamsWhoEnded += 1;
		}
	}


	/// <summary>
	/// Based on the number of elements in the Json file set verticalFrames and horizontalFrames.
	/// </summary>
	private void AdaptFieldDimensions()
	{
		int len = frames.Length;

		if (len % 3 == 0) {
			verticalFrames = 3;
			horizontalFrames = len / 3;
		} else if (len % 4 == 0) {
			verticalFrames = 4;
			horizontalFrames = len / 4;
		}
	}




}
