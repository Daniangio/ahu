﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class FrameData {

	public int id;
	public bool isCorrect;
	public string spriteName;
	public string color;
}
