﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizeSprite : MonoBehaviour {

	private SpriteRenderer sr;
	public float dimension_horizontal;
	public float dimension_vertical;
	public float transparency;
	public float size;
	public bool rotate;
	public float rotationSpeed;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		ResizeSpriteToScreen (sr, size);
		sr.color = new Color (1f, 1f, 1f, transparency);
	}

	private void ResizeSpriteToScreen(SpriteRenderer sr, float size) {
		if (sr == null)
			return;

		Vector2 local_sprite_size = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

		float frame_width = dimension_horizontal;
		float frame_height = dimension_vertical;
		float myScale_x;
		float myScale_y;
		if (frame_height == 0) {
			myScale_x = (1 / local_sprite_size.x) * frame_width * size;
			myScale_y = myScale_x;
		} else {
			myScale_x = (1 / local_sprite_size.x) * frame_width * size;
			myScale_y = (1 / local_sprite_size.y) * frame_height * size;
		}
		sr.transform.localScale = new Vector3 (myScale_x, myScale_y, 1);
	}

	void Update ()
	{
		if (rotate) {
			transform.Rotate (Vector3.forward * Time.deltaTime * rotationSpeed);
		}
	}
}
