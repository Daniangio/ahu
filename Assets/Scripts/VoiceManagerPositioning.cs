﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VoiceManagerPositioning : MonoBehaviour {

	public PositioningManager positioningManager;
	public AudioSource source;
	public AudioClip[] audios;


	void Start () {
		//source = GetComponent<AudioSource>();
	}

	public void StartAudio (int audioId)
	{
		source.PlayOneShot (audios [audioId]);
//		source.clip = audios[audioId];
//		source.Play();
	}

	public void FinalAudio (int audioId){
		StartCoroutine (PlayAudio(audioId));
	}

	IEnumerator PlayAudio (int audioId)
	{
		source.clip = audios[audioId];
		source.Play();
		yield return new WaitWhile (() => source.isPlaying);
		positioningManager.NextScene ();
	}
}
