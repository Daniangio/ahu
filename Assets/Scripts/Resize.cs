﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resize : MonoBehaviour {

	private SpriteRenderer sr;
	public Camera cam;
	public float border_top;
	public float border_bottom;
	public float border_left;
	public float border_right;
	public float transparency;
	public float size;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		ResizeSpriteToScreen (sr, size);
		sr.color = new Color (1f, 1f, 1f, transparency);
	}

	private void ResizeSpriteToScreen(SpriteRenderer sr, float size) {
		if (sr == null)
			return;

		var worldScreenHeight = cam.orthographicSize * 2;
		var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		Vector2 local_sprite_size = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

		float frame_width = (worldScreenWidth - border_left - border_right);
		float frame_height = (worldScreenHeight - border_top - border_bottom);
		float myScale_y = (1 / local_sprite_size.y) * frame_height * size;
		float myScale_x = (1 / local_sprite_size.x) * frame_width * size;
		sr.transform.localScale = new Vector3 (myScale_x, myScale_y, 1);

		float offset_x = cam.transform.position.x + border_right - border_left;
		float offset_y = cam.transform.position.y + border_bottom - border_top;

		transform.position = new Vector3 (offset_x, offset_y, 0);
	}
}
