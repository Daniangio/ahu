﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using HueLights;


public class ChoiceStatus : MonoBehaviour {
	
	static public string story = "Story";
	static public int level = 0;
	static public string mode;
	static public int progressionStory = 0;
	static public int numberOfPlayers = 3;
	static public int teamPoints_1 = 0;
	static public int teamPoints_2 = 0;
	static public float timeTeam_1 = 0.0f;
	static public float timeTeam_2 = 0.0f;
	static public int winner = 0;


	/// <summary>
	/// Set resolution of the display and activate the second display
	/// </summary>
	void Start()
	{
		Cursor.visible = false;
		Screen.SetResolution (1024, 768, true);

		if (Display.displays.Length > 1)
			Display.displays [1].Activate ();
	}


	public static string getStory() {
		return story;
	}


	public static void setStory(string s) {
		story = s;
	}

	public static string getMode() {
		return mode;
	}


	public static void setMode(string m) {
		mode = m;
	}

	public static void setProgressionStory(int ps) {
		progressionStory = ps;
	}


	public static int getProgressionStory() {
		return progressionStory;
	}


	public static void setLevel(int l) {
		level = l;
	}


	public static void LoadNextStoryScene() {
		string sceneName = "sceneName";
		if (story == "Wood") {
			sceneName = "WoodStory1";
		}
		SceneManager.LoadScene (sceneName);
	}


	/// <summary>
	/// In the case of story it loads the game scene and if games are finished it loads the reward scene.
	/// </summary>
	public static void LoadNextGameScene() {
		string sceneName;
		progressionStory += 1;
		if (story == "Wood") {
			sceneName = "Learning_1";
			if (progressionStory == 9) {
				sceneName = "Reward";
				ChoiceStatus.DecreteWinner ();
			}
			SceneManager.LoadScene (sceneName);
		}
		if (story == "Memo") {
			ChoiceStatus.mode = "Collaborative"; //da togliere
			if (ChoiceStatus.mode == "Collaborative") {
				sceneName = "Memo_Grid";
				if (progressionStory == 3) {
					sceneName = "Reward";
					ChoiceStatus.DecreteWinner ();
				}
				SceneManager.LoadScene (sceneName);
			}
			if (ChoiceStatus.mode == "Competitive") {
				sceneName = "Memo_Grid";
				if (progressionStory == 2) {
					sceneName = "Reward";
					ChoiceStatus.DecreteWinner ();
				}
				SceneManager.LoadScene (sceneName);
			}
		}
	}


	/// <summary>
	/// Decretes the winner so it modifies the variable winner.
	/// </summary>
	public static void DecreteWinner ()
	{
		if (teamPoints_1 > teamPoints_2)
			winner = 1;
		else if (teamPoints_2 > teamPoints_1)
			winner = 2;
		else
			winner = 0;
	}

	public static void DecreteFastest(){
		if (timeTeam_1 < timeTeam_2) {
			teamPoints_1 = 1;
			teamPoints_2 = 0;
		}
		if (timeTeam_2 < timeTeam_1) {
			teamPoints_2 = 1;
			teamPoints_1 = 0;
		}
		if (timeTeam_1 == timeTeam_2) {
			teamPoints_1 = 1;
			teamPoints_2 = 1;
		}
	}
		
}