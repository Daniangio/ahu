﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VoiceManager : MonoBehaviour {

	public StoryManager storyManager;
	AudioSource source;
	public AudioClip[] audios;

	void Start () {
		source = GetComponent<AudioSource>();
	}

	public void StartAudio (int audioId)
	{
		StartCoroutine (PlayAudio(audioId));
	}

	IEnumerator PlayAudio (int audioId)
	{
		source.clip = audios[audioId];
		source.Play();
		yield return new WaitWhile (() => source.isPlaying);
		storyManager.NextScene ();
	}

}
