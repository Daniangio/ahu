﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionManager : MonoBehaviour {


	/// <summary>
	/// Based on the game and progression story, gets the right json files.
	/// </summary>
	/// <returns>The json files.</returns>
	public static string[] GetJsonFiles(int field)
	{
		string[] dataPoolFiles = new string[1];

		if (ChoiceStatus.story == "Wood") {
			switch (ChoiceStatus.progressionStory) {
			case 1:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "vestiti_1.json";
				break;
			case 2:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "vestiti_2.json";
				break;
			case 3:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "animali_1.json";
				break;
			case 4:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "animali_2.json";
				break;
			case 5:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "rifiuti_1.json";
				break;
			case 6:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "rifiuti_2.json";
				break;
			case 7:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "ingredienti_1.json";
				break;
			case 8:
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "ingredienti_2.json";
				break;
			}
		}
		if (ChoiceStatus.story == "Memo") {
			switch (ChoiceStatus.level) {
			case 1: 
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "memoAnimali.json"; // con 4 elementi dentro
				break;
			case 2: 
				dataPoolFiles = new string[1];
				dataPoolFiles [0] = "memoAnimali2.json"; // con 6 elementi dentro
				break;
			case 3: 
				if (field == 1) {
					dataPoolFiles = new string[1];
					dataPoolFiles [0] = "memoAnimali3.json"; // con 6 elementi dentro ma in base al field cambia
				} else {
					dataPoolFiles = new string[1];
					dataPoolFiles [0] = "memoAnimali3Correlati.json"; // con 6 elementi dentro ma in base al field cambia
				}
				break;
			}
		}

		return dataPoolFiles;
	}


}
