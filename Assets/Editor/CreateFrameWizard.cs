﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class CreateFrameWizard : ScriptableWizard {

	public string frameName = "New Frame";
	public bool isCorrect;
	public Sprite elementSprite;

	[MenuItem("My Tools/Create Frame...")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<CreateFrameWizard> ("Create Frame", "Create new", "Update selected");
	}

	void OnWizardCreate()
	{
		GameObject framePrefab = new GameObject ();
		framePrefab.name = frameName;

		FrameScr frameScr = framePrefab.AddComponent<FrameScr> ();
		frameScr.isCorrect = isCorrect;

		GameObject elementGO = new GameObject ();
		elementGO.name = "Element";
		elementGO.transform.parent = framePrefab.transform;
		elementGO.AddComponent<SpriteRenderer> ();
		elementGO.GetComponent<SpriteRenderer>().sprite = elementSprite;

		frameScr.element = elementGO.GetComponent<SpriteRenderer> ();

	}

	void OnWizardOtherButton()
	{
		if (Selection.activeTransform != null) {

			FrameScr frameScr = Selection.activeTransform.GetComponent<FrameScr> ();

			if (frameScr != null) {
				frameScr.isCorrect = isCorrect;
				frameScr.element.sprite = elementSprite;
			}
		}
	}

}