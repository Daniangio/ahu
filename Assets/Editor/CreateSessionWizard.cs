﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateSessionWizard : ScriptableWizard {

	public string sessionName = "New Session";
	public GameObject[] framesGO;
	private FrameData[] frames;
	public string message = "Insert a message to display";
	
	[MenuItem("My Tools/Create Session...")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<CreateSessionWizard> ("Create Session", "Create new", "Update selected");
	}

	void OnWizardCreate()
	{
		GameObject session = new GameObject ();
		session.name = sessionName;
		GameDataScr gameData = session.AddComponent<GameDataScr> ();

		//Pick the FrameScr of the object and flush its parameters to the serializable FrameData
		FrameScr frameScr;
		FrameData frameData;
		frames = new FrameData[framesGO.Length];
		for (int i=0; i < framesGO.Length; i += 1)
		{
			frameScr = framesGO [i].GetComponent<FrameScr> ();
			frameData = new FrameData ();
			frameData.isCorrect = frameScr.isCorrect;
			frameData.spriteName = frameScr.element.sprite.name;
			frames [i] = frameData;
		}

		//Update the GameData with the correct serializable parameters
		gameData.frames = new FrameData[framesGO.Length];
		for (int i = 0; i < framesGO.Length; i += 1) {
			gameData.frames [i] = frames [i];
		}
		gameData.message = message;
		gameData.tag = "Session";
	}

	//TODO
	void OnWizardOtherButton()
	{
		/*if (Selection.activeTransform != null) {

			GameObject gameSession = Selection.activeTransform.gameObject;
			gameSession.name = sessionName;
			GameData gameData = new GameData ();

			if (gameData != null) {
				frames = new FrameData[framesGO.Length];
				for (int i=0; i < framesGO.Length; i += 1)
				{
					frames [i] = framesGO [i].GetComponent<FrameData> ();
				}
				gameData.frames = frames;
				gameData.message = message;
			}
		}*/
	}

}
