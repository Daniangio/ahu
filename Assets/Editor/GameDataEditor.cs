﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

public class GameDataEditor : EditorWindow {

    public GameData gameData;
	public GameObject source;
	public string gameDataProjectFileName;

	[MenuItem ("Window/Game Data Editor")]
	static void Init()
	{
		GameDataEditor window = (GameDataEditor)EditorWindow.GetWindow (typeof(GameDataEditor));
		window.Show ();
	}

	void OnGUI()
	{
		GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
		source = (GameObject)EditorGUILayout.ObjectField (source, typeof(Object), true);
		gameDataProjectFileName = EditorGUILayout.TextField ("data");

		if (gameData != null) {
			SerializedObject serializedObject = new SerializedObject (this);
			SerializedProperty serializedProperty = serializedObject.FindProperty ("gameData");

			EditorGUILayout.PropertyField (serializedProperty, true);

			serializedObject.ApplyModifiedProperties ();

			if (GUILayout.Button ("Save data")) {
				SaveGameData ();
			}
		}

		if (GUILayout.Button ("Load data from Json")) {
			LoadGameDataJson ();
		}

		if (GUILayout.Button ("Load data from class")) {
			LoadGameDataClass ();
		}
	}

	private void LoadGameDataJson()
    {
		string filePath = Application.dataPath + "/StreamingAssets/" + gameDataProjectFileName;

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
			
        }
    }

	private void LoadGameDataClass()
	{
		GameDataScr sourceData = source.GetComponent<GameDataScr> ();
		if (sourceData != null) {
			gameData = new GameData ();
			gameData.frames = sourceData.frames;
			gameData.message = sourceData.message;
		}
		else
		{
			
		}
	}

    private void SaveGameData()
    {
        string dataAsJson = JsonUtility.ToJson(gameData);
		string filePath = Application.dataPath + "/StreamingAssets/" + gameDataProjectFileName + ".json";
        File.WriteAllText(filePath, dataAsJson);
    }
}
